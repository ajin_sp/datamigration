-- Add FB2 content on 9/05/2016 --


--- FB2 fuction end--
--
-- CRS Data Migration Script
--


-- Query #1 - Insert Non-Daily Rates Investments
-- Run in CRS MS Access database to generate the TSQL INSERT statements for the new CRS SQL 2005 database

select
'INSERT INTO Investment ' + 
'(FundId, InvestmentCode, InvestmentDescription, InterimRateCode, DeclaredRateCode, ' +
'GPenCode, GPenFundCode, IsDefault, PWRRule, SwitchRule, XmlFlag, InvestmentType, SuperbDeclaredCode, ' +
'CalibreDeclaredCode, SuperbInterimCode, CalibreInterimCode, DateCreated, CreatedBy) ' +
'SELECT FundId, ' +
'''' + Investment_Code + ''', ' +
'''' + REPLACE(Investment_Description, '''', '''''') + ''', ' +
'''' + Interim_Rate_Code + ''', ' +
'''' + Declared_Rate_Code + ''', ' +
iif(isnull(GpenCode), '0', cstr(GpenCode)) + ', ' +
'''' + iif(isnull(GpenFundID), '', GpenFundID) + ''', ' +
cstr([Default]) + ', ' +
'''' + iif(isnull(PWPRule), '', PWPRule) + ''', ' +
'''' + iif(isnull(SWITCHRule), '', SWITCHRule) + ''', ' +
cstr([XML_Flag]) + ', ' +
iif(isnull(Investment_Type), 'null', '''' + cstr(Investment_Type) + '''') + ', ' +
'''' + '' + ''', ' +
'''' + '' + ''', ' +
'''' + '' + ''', ' +
'''' + '' + ''', ' +
'getdate(), ''System'' ' + 
'FROM Fund WHERE FundName=''' + [Fund] + '''' 
from
tblInvestmentCodes



-- Query #2 - Insert Non-Daily Rates Fund Rules
-- Run in CRS MS Access database to generate the TSQL INSERT statements for the new CRS SQL 2005 database

select
'INSERT INTO FundRule ' + 
'(InvestmentId, RateTypeId, RatePeriodId, LastDate, RuleCode, DefaultRule, ' + 
'DefaultRuleCode, R2RuleCode, SuperbRuleCode, CalibreRuleCode, R2Dec, SuperbDec, ' + 
'CalibreDec, R2RuleType, SuperbRuleType, CalibreRuleType, CreatedBy, DateCreated) ' +
'SELECT TOP 1 InvestmentId, ' +
'case ''' + RateType + ''' when ''DEC'' then 1 when ''INT'' then 2 when ''SPC'' then 3 end, ' +
'case ''' + RatePeriod + ''' when ''pw'' then 1 when ''pm'' then 2 when ''pa'' then 3 when ''ph'' then 6 end, ' +
'''' + format(dLastDate, 'yyyy-mm-dd') + ''', ' +
'''' + RuleCode + ''', ' +
'''' + Default + ''', ' +
'''' + DefaultRuleCode + ''', ' +
'''' + iif(isnull(R2RuleCode), '', R2RuleCode) + ''' , ' +
'''' + iif(isnull(SuperbRuleCode), '', SuperbRuleCode) + ''', ' +
'''' + iif(isnull(CalibreRuleCode), '', CalibreRuleCode) + ''', ' +
'''' + iif(isnull(R2Dec), '', R2Dec) + ''', ' +
'''' + iif(isnull(SuperbDec), '', SuperbDec) + ''', ' +
'''' + iif(isnull(CalibreDec), '', cstr(CalibreDec)) + ''', ' +
'''' + iif(isnull(R2RuleType), '', R2RuleType) + ''', ' +
'''' + iif(isnull(SuperbRuleType), '', SuperbRuleType) + ''', ' +
'''' + iif(isnull(CalibreRuleType), '', CalibreRuleType) + ''', ' + 
'''System'', getdate() ' + 
'FROM Investment WHERE InvestmentCode=''' + Investment_Code + '''' 
from
tblFundRules



-- Query #3 - Insert Dummy Workflow and assign to temp fund (AusSuper)
-- Run in CRS SQL 2005 database

INSERT INTO [dbo].[WorkFlow]
           ([AssignTo]
           ,[WorkFlowStatusId]
           ,[FundId]
           ,[DateTime]
           ,[DateExported]
           ,[ReconciledR2]
           ,[ReconciledSuperb]
           ,[ReconciledCalibre])
     SELECT 'systemp',
				(select WorkFlowStatusId from dbo.WorkFlowStatus where WorkFlowStatusCode='Exported'),
				FundId,getdate(),getdate(),1,1,0 
				FROM dbo.Fund 
				WHERE FundName='AUSSUPER'



-- Query #4 - Insert Non-Daily Rates
-- Run in CRS SQL 2005 database to migrate data from the SPIT data dump table in SQL 2005 to the CRS rates table in SQL 2005
-- Note: Excludes AusSuper, APF/AUSfund, AUSTQ due to missing data (as of Nov 2009)
-- Note: Uses dummy workflow and temp fund

BEGIN TRY
	BEGIN TRANSACTION

	INSERT INTO Rate 
		(FundRuleId, InvestmentId, WorkFlowId, RatePeriodId, RateTypeId, StartDate, 
		EndDate, SetDate, CreditingRate, ConvertedRate, PensionRate, MonthlyUseRate, R2Rate, 
		SuperbRate, CalibreRate, CreatedBy, DateCreated, ModifiedBy, DateModified, Cancelled) 
	select FundRuleId, InvestmentId, (select top 1 WorkFlowId from dbo.WorkFlow order by WorkFlowId desc), RatePeriodId, RateTypeId, START_DATE,
		END_DATE, EFFECTIVE_DATE, RATE, RATE, RATE, RATE, RATE,
		RATE, RATE, CREATED_BY, CREATED_DATE, MOD_BY, MOD_DATE, case when CANCELLED_BY is not null then 1 else 0 end
	from
	(
		select 
		f.FundId, i.InvestmentId, rt.RateTypeId, spit.START_DATE, spit.END_DATE, 
		spit.EFFECTIVE_DATE, spit.RATE, spit.CREATED_BY, spit.CREATED_DATE,
		spit.AUTHORISED_BY MOD_BY, spit.AUTHORISED_DATE MOD_DATE, spit.CANCELLED_BY, 
		(select top 1 fr.FundRuleId from FundRule fr
			where fr.InvestmentId=i.InvestmentId and fr.RateTypeId=rt.RateTypeId
			order by fr.LastDate desc) FundRuleId,
		(select top 1 fr.RatePeriodId from FundRule fr
			where fr.InvestmentId=i.InvestmentId and fr.RateTypeId=rt.RateTypeId
			order by fr.LastDate desc) RatePeriodId
		from SPIT_Rates spit, Fund f, Investment i, RateType rt
		where
		cast(spit.FUND_CLTARR_ID as nvarchar(10))=f.R2FundId
		and upper(spit.INVESTMENT_OPTION_CODE)=upper(i.InvestmentCode)
		and upper(rt.RateTypeInterfaceCode)=upper(spit.RATE_TYPE_CODE)
		and spit.AUTHORISED_DATE is not null
		and datediff(day, '2008-07-01', spit.AUTHORISED_DATE) >= 0
	) as subq
	where FundRuleId is not null and RatePeriodId is not null
	and FundId not in (57, 5, 15) --exclude AusSuper, APF/AUSfund, AUSTQ
	order by FundId, InvestmentId, RateTypeId, RatePeriodId

	COMMIT TRANSACTION
END TRY
BEGIN CATCH

	print 'Error occurred'

	ROLLBACK TRANSACTION
END CATCH



-- Query #5 - Create multiple Workflows for all daily rates
-- Run in CRS SQL 2005 database

BEGIN TRY
	BEGIN TRANSACTION

		declare @FundId bigint
		declare @AuthDate datetime
		declare @CreatedBy nvarchar(10)
		declare @DateCreated datetime

		declare @ConfirmedWorkFlowStatusId bigint
		declare @NewWorkFlowId bigint
		declare @DummyWorkFlowId bigint

		select @DummyWorkFlowId=max(WorkFlowId) from WorkFlow

		select @ConfirmedWorkFlowStatusId=WorkFlowStatusId
		from WorkFlowStatus where WorkFlowStatusCode='Confirmed'

		Declare AllAuthDates Cursor FAST_FORWARD
		For 
			select f.FundId, DATEADD(dd, 0, DATEDIFF(dd, 0, spit.AUTHORISED_DATE)) AuthDate,
				spit.CREATED_BY CreatedBy, DATEADD(dd, 0, DATEDIFF(dd, 0, spit.CREATED_DATE)) DateCreated
			from dbo.SPIT_Rates spit, dbo.Fund f
			where f.R2FundId=cast(spit.FUND_CLTARR_ID as nvarchar(10))
			group by f.FundId, DATEADD(dd, 0, DATEDIFF(dd, 0, spit.AUTHORISED_DATE)), 
				spit.CREATED_BY, DATEADD(dd, 0, DATEDIFF(dd, 0, spit.CREATED_DATE))
			order by DATEADD(dd, 0, DATEDIFF(dd, 0, spit.AUTHORISED_DATE)), f.FundId

		Open AllAuthDates;

		Fetch Next from AllAuthDates Into 
			@FundId, @AuthDate, @CreatedBy, @DateCreated

		WHILE (@@fetch_status <> -1)
		BEGIN

			INSERT INTO [dbo].[WorkFlow]
					   ([AssignTo]
					   ,[WorkFlowStatusId]
					   ,[FundId]
					   ,[DateTime]
					   ,[DateExported]
					   ,[ReconciledR2]
					   ,[ReconciledSuperb]
					   ,[ReconciledCalibre])
				 VALUES
					   (@CreatedBy,
						@ConfirmedWorkFlowStatusId,
						@FundId,
						@DateCreated,
						@AuthDate,
						1,
						1,
						0)

			select @NewWorkFlowId = SCOPE_IDENTITY()

			update Rate set WorkFlowId=@NewWorkFlowId 
			where WorkFlowId=@DummyWorkFlowId
				and DATEADD(dd, 0, DATEDIFF(dd, 0, SetDate))=DATEADD(dd, 0, DATEDIFF(dd, 0, @AuthDate))

			-- Fetch next row

			Fetch Next from AllAuthDates Into 
				@FundId, @AuthDate, @CreatedBy, @DateCreated
		END;

		-- Cleanup Cursor

		Close AllAuthDates;
		DeAllocate AllAuthDates;

		--delete from WorkFlow where AssignTo='systemp'

	COMMIT TRANSACTION
END TRY
BEGIN CATCH

	Close AllAuthDates;
	DeAllocate AllAuthDates;
	
	print 'Error occurred: ' + cast(ERROR_NUMBER() as nvarchar(50)) + ' - ' + ERROR_MESSAGE();

	ROLLBACK TRANSACTION
END CATCH

