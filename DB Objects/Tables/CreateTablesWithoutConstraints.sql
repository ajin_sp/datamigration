
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContentZip]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ContentZip](
	[ContentZip] [varbinary](max) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SystemConfig]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SystemConfig](
	[ConfigId] [int] IDENTITY(1,1) NOT NULL,
	[ConfigName] [nvarchar](50) NOT NULL,
	[ConfigValue] [nvarchar](max) NOT NULL,
	[Updateable] [bit] NOT NULL CONSTRAINT [DF_SystemConfig_Updateable]  DEFAULT ((1)),
 CONSTRAINT [PK_Config] PRIMARY KEY CLUSTERED 
(
	[ConfigId] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExportQueue]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ExportQueue](
	[SeqId] [bigint] IDENTITY(1,1) NOT NULL,
	[StatusId] [tinyint] NOT NULL,
	[ErrorMessage] [nvarchar](max) NULL,
	[ErrorRateId] [bigint] NULL,
	[YTDRatesZip] [varbinary](max) NULL,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_ExportQueue_DateCreated]  DEFAULT (getdate()),
	[DateLastUpdated] [datetime] NULL,
 CONSTRAINT [PK_ExportLog] PRIMARY KEY CLUSTERED 
(
	[SeqId] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Fund]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Fund](
	[FundId] [bigint] IDENTITY(1,1) NOT NULL,
	[FundName] [nvarchar](20) NOT NULL,
	[R2FundId] [nvarchar](10) NOT NULL,
	[DailyRatesEAIWS] [nvarchar](200) NULL,
	[TargetDateShiftDays] [smallint] NOT NULL CONSTRAINT [DF_Fund_TargetDateShiftDays]  DEFAULT ((0)),
	[FYTDCorporateValidation] [bit] NOT NULL CONSTRAINT [DF_Fund_FYTDCorporateValidation]  DEFAULT ((0)),
	[FYTDPensionValidation] [bit] NOT NULL CONSTRAINT [DF_Fund_FYTDPensionValidation]  DEFAULT ((0)),
	[ValidationTolerance] [decimal](18, 3) NOT NULL CONSTRAINT [DF_Fund_ValidationTolerance]  DEFAULT ((0)),
	[VerifierEmailAddress] [nvarchar](200) NULL,
	[ApproverEmailAddress] [nvarchar](200) NULL,
	[Disabled] [bit] NOT NULL CONSTRAINT [DF_Fund_Disabled]  DEFAULT ((0)),
	[CreatedBy] [nvarchar](10) NULL,
	[DateCreated] [datetime] NULL CONSTRAINT [DF_Fund_DateCreated]  DEFAULT (getdate()),
	[ModifiedBy] [nvarchar](10) NULL,
	[DateModified] [datetime] NULL,
 CONSTRAINT [PK_Funds] PRIMARY KEY CLUSTERED 
(
	[FundId] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RatePeriod]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RatePeriod](
	[RatePeriodId] [bigint] IDENTITY(1,1) NOT NULL,
	[RatePeriodCode] [nvarchar](10) NOT NULL,
	[RatePeriodDescription] [nvarchar](100) NOT NULL,
	[CreatedBy] [nvarchar](10) NOT NULL,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_RatePeriod_DateCreated]  DEFAULT (getdate()),
	[ModifiedBy] [nvarchar](10) NULL,
	[DateModified] [datetime] NULL,
 CONSTRAINT [PK_RatePeriod] PRIMARY KEY CLUSTERED 
(
	[RatePeriodId] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XmlMethodParameters]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[XmlMethodParameters](
	[SeqId] [nvarchar](50) NULL,
	[XmlNodeName] [nvarchar](max) NULL,
	[XmlValue] [nvarchar](max) NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Task]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Task](
	[TaskId] [bigint] IDENTITY(1,1) NOT NULL,
	[TaskName] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[ProcessMethodName] [nvarchar](50) NOT NULL,
	[Disabled] [bit] NOT NULL,
	[DateNextRun] [datetime] NULL,
	[DateLastRun] [datetime] NULL,
	[CreatedBy] [nvarchar](10) NOT NULL,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_Task_DateCreated]  DEFAULT (getdate()),
	[ModifiedBy] [nvarchar](10) NULL,
	[DateModified] [datetime] NULL,
 CONSTRAINT [PK_Task] PRIMARY KEY CLUSTERED 
(
	[TaskId] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RateType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RateType](
	[RateTypeId] [bigint] IDENTITY(1,1) NOT NULL,
	[RateTypeCode] [nvarchar](10) NOT NULL,
	[RateTypeInterfaceCode] [nvarchar](15) NOT NULL,
	[RateTypeDescription] [nvarchar](200) NOT NULL,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_RateTypes_DateCreated]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](10) NOT NULL,
	[DateModified] [datetime] NULL,
	[ModifiedBy] [nvarchar](10) NULL,
 CONSTRAINT [PK_RateTypes] PRIMARY KEY CLUSTERED 
(
	[RateTypeId] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WorkFlowStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[WorkFlowStatus](
	[WorkFlowStatusId] [bigint] IDENTITY(1,1) NOT NULL,
	[WorkFlowSequenceId] [bigint] NOT NULL,
	[WorkFlowStatusCode] [nvarchar](50) NOT NULL,
	[WorkFlowStatusDescription] [nvarchar](100) NOT NULL,
	[CreatedBy] [nchar](10) NOT NULL,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_WorkFlowStatus_DateCreated]  DEFAULT (getdate()),
	[ModifiedBy] [nchar](10) NULL,
	[DateModified] [datetime] NULL,
 CONSTRAINT [PK_WorkFlowStatus] PRIMARY KEY CLUSTERED 
(
	[WorkFlowStatusId] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReconTempRates]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ReconTempRates](
	[RowId] [bigint] IDENTITY(1,1) NOT NULL,
	[ExternalSystemId] [smallint] NOT NULL,
	[RateId] [bigint] NULL,
	[FundId] [bigint] NULL,
	[InvestmentCode] [nvarchar](50) NOT NULL,
	[RateTypeCode] [nvarchar](10) NOT NULL,
	[ReconRateR2] [nvarchar](50) NULL,
	[ReconRateSuperb] [nvarchar](50) NULL,
	[ReconRateCalibre] [nvarchar](50) NULL,
	[SetDate] [datetime] NOT NULL,
	[ExportDate] [datetime] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_ReconTempRates] PRIMARY KEY CLUSTERED 
(
	[RowId] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransitionRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TransitionRate](
	[TransitionRateId] [bigint] IDENTITY(1,1) NOT NULL,
	[Guid] [nvarchar](40) NOT NULL,
	[TransitionRateTypeId] [bigint] NOT NULL,
	[TransitionRatePeriodId] [bigint] NOT NULL,
	[InvestmentId] [bigint] NULL,
	[FundRuleId] [bigint] NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[SetDate] [datetime] NOT NULL CONSTRAINT [DF_TransitionRate_SetDate]  DEFAULT (getdate()),
	[CreditingTransitionRate] [decimal](18, 3) NULL,
	[PensionTransitionRate] [decimal](18, 3) NULL,
	[ConvertedTransitionRate] [decimal](18, 3) NULL,
	[TransitionMonthlyUseRate] [nvarchar](50) NULL,
	[R2TransitionRate] [nvarchar](50) NULL,
	[SuperbTransitionRate] [nvarchar](50) NULL,
	[CalibreTransitionRate] [nvarchar](50) NULL,
	[CorporateFYTDRate] [decimal](18, 3) NULL,
	[PensionFYTDRate] [decimal](18, 3) NULL,
	[Cancelled] [bit] NULL,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_TransitionRate_DateCreated]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](10) NOT NULL,
	[DateModified] [datetime] NULL,
	[ModifiedBy] [nvarchar](10) NULL,
 CONSTRAINT [PK_TransitionRate] PRIMARY KEY CLUSTERED 
(
	[TransitionRateId] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Rate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Rate](
	[RateId] [bigint] IDENTITY(1,1) NOT NULL,
	[InvestmentId] [bigint] NULL,
	[FundRuleId] [bigint] NULL,
	[WorkFlowId] [bigint] NULL,
	[RatePeriodId] [bigint] NULL,
	[RateTypeId] [bigint] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[SetDate] [datetime] NOT NULL CONSTRAINT [DF_Rates_SetDate]  DEFAULT (getdate()),
	[CreditingRate] [decimal](18, 3) NULL,
	[ConvertedRate] [decimal](18, 3) NULL,
	[PensionRate] [decimal](18, 3) NULL,
	[MonthlyUseRate] [nvarchar](50) NULL,
	[R2Rate] [nvarchar](50) NULL,
	[SuperbRate] [nvarchar](50) NULL,
	[CalibreRate] [nvarchar](50) NULL,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_Rates_DateCreated]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](10) NOT NULL,
	[DateModified] [datetime] NULL,
	[ModifiedBy] [nvarchar](10) NULL,
	[Cancelled] [bit] NULL CONSTRAINT [DF_Rate_Cancelled]  DEFAULT ((0)),
	[Deleted] [bit] NULL CONSTRAINT [DF_Rate_Deleted]  DEFAULT ((0)),
 CONSTRAINT [PK_Rates] PRIMARY KEY CLUSTERED 
(
	[RateId] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FundRule]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FundRule](
	[FundRuleId] [bigint] IDENTITY(1,1) NOT NULL,
	[InvestmentId] [bigint] NOT NULL,
	[RateTypeId] [bigint] NOT NULL,
	[RatePeriodId] [bigint] NOT NULL,
	[LastDate] [datetime] NOT NULL,
	[RuleCode] [nvarchar](1) NOT NULL,
	[DefaultRule] [nvarchar](20) NOT NULL,
	[DefaultRuleCode] [nvarchar](10) NOT NULL,
	[R2RuleCode] [nvarchar](10) NULL,
	[SuperbRuleCode] [nvarchar](10) NULL,
	[CalibreRuleCode] [nvarchar](10) NULL,
	[R2Dec] [nvarchar](10) NULL,
	[SuperbDec] [nvarchar](10) NULL,
	[CalibreDec] [nvarchar](10) NULL,
	[R2RuleType] [nvarchar](10) NULL,
	[SuperbRuleType] [nvarchar](10) NULL,
	[CalibreRuleType] [nvarchar](10) NULL,
	[CreatedBy] [nvarchar](10) NULL,
	[DateCreated] [datetime] NULL,
	[ModifiedBy] [nvarchar](10) NULL,
	[DateModified] [datetime] NULL,
 CONSTRAINT [PK_FundRule] PRIMARY KEY CLUSTERED 
(
	[FundRuleId] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RateAudit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RateAudit](
	[RateAuditId] [bigint] IDENTITY(1,1) NOT NULL,
	[RateId] [bigint] NOT NULL,
	[RateTypeId] [bigint] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[SetDate] [datetime] NOT NULL,
	[CreditingRate] [decimal](18, 3) NULL,
	[ConvertedRate] [decimal](18, 3) NULL,
	[RatePeriodId] [bigint] NULL,
	[MonthlyUseRate] [nchar](100) NULL,
	[R2Rate] [nchar](100) NULL,
	[SuperbRate] [nchar](100) NULL,
	[CalibreRate] [nchar](100) NULL,
	[InvestmentId] [bigint] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](10) NOT NULL,
	[DateModified] [datetime] NULL,
	[ModifiedBy] [nvarchar](10) NULL,
	[WorkFlowHistoryId] [bigint] NULL,
	[FundRuleId] [bigint] NOT NULL,
	[Cancelled] [bit] NULL,
 CONSTRAINT [PK_RateAudit] PRIMARY KEY CLUSTERED 
(
	[RateAuditId] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserFunds]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserFunds](
	[RecordId] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](10) NOT NULL,
	[FundId] [bigint] NOT NULL,
	[CreatedBy] [nvarchar](10) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[RecordId] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Investment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Investment](
	[InvestmentId] [bigint] IDENTITY(1,1) NOT NULL,
	[FundId] [bigint] NOT NULL,
	[InvestmentCode] [nvarchar](50) NULL,
	[InvestmentDescription] [nvarchar](150) NULL,
	[InterimRateCode] [nvarchar](30) NOT NULL,
	[DeclaredRateCode] [nvarchar](30) NOT NULL,
	[GPenCode] [bigint] NULL,
	[GPenFundCode] [nvarchar](50) NULL,
	[IsDefault] [bit] NULL CONSTRAINT [DF_Investments_IsDefault]  DEFAULT ((0)),
	[PWRRule] [nvarchar](1) NULL,
	[SwitchRule] [nvarchar](1) NULL,
	[XmlFlag] [bit] NULL CONSTRAINT [DF_Investments_XmlFlag]  DEFAULT ((0)),
	[InvestmentType] [nvarchar](2) NULL,
	[SuperbDeclaredCode] [nvarchar](30) NULL,
	[CalibreDeclaredCode] [nvarchar](30) NULL,
	[SuperbInterimCode] [nvarchar](30) NULL,
	[CalibreInterimCode] [nvarchar](30) NULL,
	[DateCreated] [datetime] NULL CONSTRAINT [DF_Investments_DateCreated]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](10) NULL,
	[DateModified] [datetime] NULL,
	[ModifiedBy] [nvarchar](10) NULL,
 CONSTRAINT [PK_Investments] PRIMARY KEY CLUSTERED 
(
	[InvestmentId] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WorkFlow]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[WorkFlow](
	[WorkFlowId] [bigint] IDENTITY(1,1) NOT NULL,
	[AssignTo] [nvarchar](10) NOT NULL,
	[WorkFlowStatusId] [bigint] NOT NULL,
	[FundId] [bigint] NOT NULL,
	[DateTime] [datetime] NULL CONSTRAINT [DF_WorkFlow_DateTime]  DEFAULT (getdate()),
	[DateExported] [datetime] NULL,
	[ReconciledR2] [bit] NOT NULL CONSTRAINT [DF_WorkFlow_ReconciledR2]  DEFAULT ((0)),
	[ReconciledSuperb] [bit] NOT NULL CONSTRAINT [DF_WorkFlow_ReconciledSuperb]  DEFAULT ((0)),
	[ReconciledCalibre] [bit] NOT NULL CONSTRAINT [DF_WorkFlow_ReconciledCalibre]  DEFAULT ((0)),
 CONSTRAINT [PK_WorkFlow] PRIMARY KEY CLUSTERED 
(
	[WorkFlowId] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WorkFlowHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[WorkFlowHistory](
	[WorkFlowHistoryId] [bigint] IDENTITY(1,1) NOT NULL,
	[WorkFlowId] [bigint] NOT NULL,
	[WorkFlowStatusId] [bigint] NOT NULL,
	[AssignTo] [nvarchar](10) NOT NULL,
	[DateTime] [datetime] NOT NULL CONSTRAINT [DF_WorkFlowHistory_DateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_WorkFlowHistory] PRIMARY KEY CLUSTERED 
(
	[WorkFlowHistoryId] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProcessLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProcessLog](
	[LogId] [bigint] IDENTITY(1,1) NOT NULL,
	[WorkFlowId] [bigint] NULL,
	[RateId] [bigint] NULL,
	[ProcessModule] [nvarchar](100) NOT NULL,
	[LogMessage] [nvarchar](max) NOT NULL,
	[DateLogged] [datetime] NOT NULL CONSTRAINT [DF_ProcessLog_DateLogged]  DEFAULT (getdate()),
 CONSTRAINT [PK_ProcessLog] PRIMARY KEY CLUSTERED 
(
	[LogId] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Schedule]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Schedule](
	[ScheduleId] [bigint] IDENTITY(1,1) NOT NULL,
	[TaskId] [bigint] NOT NULL,
	[ExecutionTime] [nvarchar](5) NOT NULL,
	[Disabled] [bit] NOT NULL,
	[CreatedBy] [nvarchar](10) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](10) NULL,
	[DateModified] [datetime] NULL,
 CONSTRAINT [PK_Schedule] PRIMARY KEY CLUSTERED 
(
	[ScheduleId] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
