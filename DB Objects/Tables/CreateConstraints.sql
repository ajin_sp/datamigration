
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TransitionRate_FundRule]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransitionRate]'))
ALTER TABLE [dbo].[TransitionRate]  WITH CHECK ADD  CONSTRAINT [FK_TransitionRate_FundRule] FOREIGN KEY([FundRuleId])
REFERENCES [dbo].[FundRule] ([FundRuleId])
GO
ALTER TABLE [dbo].[TransitionRate] CHECK CONSTRAINT [FK_TransitionRate_FundRule]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TransitionRate_Investments]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransitionRate]'))
ALTER TABLE [dbo].[TransitionRate]  WITH CHECK ADD  CONSTRAINT [FK_TransitionRate_Investments] FOREIGN KEY([InvestmentId])
REFERENCES [dbo].[Investment] ([InvestmentId])
GO
ALTER TABLE [dbo].[TransitionRate] CHECK CONSTRAINT [FK_TransitionRate_Investments]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TransitionRate_RatePeriod]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransitionRate]'))
ALTER TABLE [dbo].[TransitionRate]  WITH CHECK ADD  CONSTRAINT [FK_TransitionRate_RatePeriod] FOREIGN KEY([TransitionRatePeriodId])
REFERENCES [dbo].[RatePeriod] ([RatePeriodId])
GO
ALTER TABLE [dbo].[TransitionRate] CHECK CONSTRAINT [FK_TransitionRate_RatePeriod]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TransitionRate_TransitionRateTypes]') AND parent_object_id = OBJECT_ID(N'[dbo].[TransitionRate]'))
ALTER TABLE [dbo].[TransitionRate]  WITH CHECK ADD  CONSTRAINT [FK_TransitionRate_TransitionRateTypes] FOREIGN KEY([TransitionRateTypeId])
REFERENCES [dbo].[RateType] ([RateTypeId])
GO
ALTER TABLE [dbo].[TransitionRate] CHECK CONSTRAINT [FK_TransitionRate_TransitionRateTypes]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Rate_FundRule]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rate]'))
ALTER TABLE [dbo].[Rate]  WITH CHECK ADD  CONSTRAINT [FK_Rate_FundRule] FOREIGN KEY([FundRuleId])
REFERENCES [dbo].[FundRule] ([FundRuleId])
GO
ALTER TABLE [dbo].[Rate] CHECK CONSTRAINT [FK_Rate_FundRule]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Rate_RatePeriod]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rate]'))
ALTER TABLE [dbo].[Rate]  WITH CHECK ADD  CONSTRAINT [FK_Rate_RatePeriod] FOREIGN KEY([RatePeriodId])
REFERENCES [dbo].[RatePeriod] ([RatePeriodId])
GO
ALTER TABLE [dbo].[Rate] CHECK CONSTRAINT [FK_Rate_RatePeriod]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Rate_WorkFlow]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rate]'))
ALTER TABLE [dbo].[Rate]  WITH CHECK ADD  CONSTRAINT [FK_Rate_WorkFlow] FOREIGN KEY([WorkFlowId])
REFERENCES [dbo].[WorkFlow] ([WorkFlowId])
GO
ALTER TABLE [dbo].[Rate] CHECK CONSTRAINT [FK_Rate_WorkFlow]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Rates_Investments]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rate]'))
ALTER TABLE [dbo].[Rate]  WITH CHECK ADD  CONSTRAINT [FK_Rates_Investments] FOREIGN KEY([InvestmentId])
REFERENCES [dbo].[Investment] ([InvestmentId])
GO
ALTER TABLE [dbo].[Rate] CHECK CONSTRAINT [FK_Rates_Investments]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Rates_RateTypes]') AND parent_object_id = OBJECT_ID(N'[dbo].[Rate]'))
ALTER TABLE [dbo].[Rate]  WITH CHECK ADD  CONSTRAINT [FK_Rates_RateTypes] FOREIGN KEY([RateTypeId])
REFERENCES [dbo].[RateType] ([RateTypeId])
GO
ALTER TABLE [dbo].[Rate] CHECK CONSTRAINT [FK_Rates_RateTypes]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FundRule_Investments]') AND parent_object_id = OBJECT_ID(N'[dbo].[FundRule]'))
ALTER TABLE [dbo].[FundRule]  WITH CHECK ADD  CONSTRAINT [FK_FundRule_Investments] FOREIGN KEY([InvestmentId])
REFERENCES [dbo].[Investment] ([InvestmentId])
GO
ALTER TABLE [dbo].[FundRule] CHECK CONSTRAINT [FK_FundRule_Investments]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FundRule_RatePeriod]') AND parent_object_id = OBJECT_ID(N'[dbo].[FundRule]'))
ALTER TABLE [dbo].[FundRule]  WITH CHECK ADD  CONSTRAINT [FK_FundRule_RatePeriod] FOREIGN KEY([RatePeriodId])
REFERENCES [dbo].[RatePeriod] ([RatePeriodId])
GO
ALTER TABLE [dbo].[FundRule] CHECK CONSTRAINT [FK_FundRule_RatePeriod]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_FundRule_RateTypes]') AND parent_object_id = OBJECT_ID(N'[dbo].[FundRule]'))
ALTER TABLE [dbo].[FundRule]  WITH CHECK ADD  CONSTRAINT [FK_FundRule_RateTypes] FOREIGN KEY([RateTypeId])
REFERENCES [dbo].[RateType] ([RateTypeId])
GO
ALTER TABLE [dbo].[FundRule] CHECK CONSTRAINT [FK_FundRule_RateTypes]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RateAudit_FundRule]') AND parent_object_id = OBJECT_ID(N'[dbo].[RateAudit]'))
ALTER TABLE [dbo].[RateAudit]  WITH CHECK ADD  CONSTRAINT [FK_RateAudit_FundRule] FOREIGN KEY([FundRuleId])
REFERENCES [dbo].[FundRule] ([FundRuleId])
GO
ALTER TABLE [dbo].[RateAudit] CHECK CONSTRAINT [FK_RateAudit_FundRule]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RateAudit_Investments]') AND parent_object_id = OBJECT_ID(N'[dbo].[RateAudit]'))
ALTER TABLE [dbo].[RateAudit]  WITH CHECK ADD  CONSTRAINT [FK_RateAudit_Investments] FOREIGN KEY([InvestmentId])
REFERENCES [dbo].[Investment] ([InvestmentId])
GO
ALTER TABLE [dbo].[RateAudit] CHECK CONSTRAINT [FK_RateAudit_Investments]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RateAudit_Rate]') AND parent_object_id = OBJECT_ID(N'[dbo].[RateAudit]'))
ALTER TABLE [dbo].[RateAudit]  WITH CHECK ADD  CONSTRAINT [FK_RateAudit_Rate] FOREIGN KEY([RateId])
REFERENCES [dbo].[Rate] ([RateId])
GO
ALTER TABLE [dbo].[RateAudit] CHECK CONSTRAINT [FK_RateAudit_Rate]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RateAudit_RatePeriod]') AND parent_object_id = OBJECT_ID(N'[dbo].[RateAudit]'))
ALTER TABLE [dbo].[RateAudit]  WITH CHECK ADD  CONSTRAINT [FK_RateAudit_RatePeriod] FOREIGN KEY([RatePeriodId])
REFERENCES [dbo].[RatePeriod] ([RatePeriodId])
GO
ALTER TABLE [dbo].[RateAudit] CHECK CONSTRAINT [FK_RateAudit_RatePeriod]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RateAudit_RateTypes]') AND parent_object_id = OBJECT_ID(N'[dbo].[RateAudit]'))
ALTER TABLE [dbo].[RateAudit]  WITH CHECK ADD  CONSTRAINT [FK_RateAudit_RateTypes] FOREIGN KEY([RateTypeId])
REFERENCES [dbo].[RateType] ([RateTypeId])
GO
ALTER TABLE [dbo].[RateAudit] CHECK CONSTRAINT [FK_RateAudit_RateTypes]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RateAudit_WorkFlowHistory]') AND parent_object_id = OBJECT_ID(N'[dbo].[RateAudit]'))
ALTER TABLE [dbo].[RateAudit]  WITH CHECK ADD  CONSTRAINT [FK_RateAudit_WorkFlowHistory] FOREIGN KEY([WorkFlowHistoryId])
REFERENCES [dbo].[WorkFlowHistory] ([WorkFlowHistoryId])
GO
ALTER TABLE [dbo].[RateAudit] CHECK CONSTRAINT [FK_RateAudit_WorkFlowHistory]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Users_Fund]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserFunds]'))
ALTER TABLE [dbo].[UserFunds]  WITH CHECK ADD  CONSTRAINT [FK_Users_Fund] FOREIGN KEY([FundId])
REFERENCES [dbo].[Fund] ([FundId])
GO
ALTER TABLE [dbo].[UserFunds] CHECK CONSTRAINT [FK_Users_Fund]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Investments_Funds]') AND parent_object_id = OBJECT_ID(N'[dbo].[Investment]'))
ALTER TABLE [dbo].[Investment]  WITH CHECK ADD  CONSTRAINT [FK_Investments_Funds] FOREIGN KEY([FundId])
REFERENCES [dbo].[Fund] ([FundId])
GO
ALTER TABLE [dbo].[Investment] CHECK CONSTRAINT [FK_Investments_Funds]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_WorkFlow_Fund]') AND parent_object_id = OBJECT_ID(N'[dbo].[WorkFlow]'))
ALTER TABLE [dbo].[WorkFlow]  WITH CHECK ADD  CONSTRAINT [FK_WorkFlow_Fund] FOREIGN KEY([FundId])
REFERENCES [dbo].[Fund] ([FundId])
GO
ALTER TABLE [dbo].[WorkFlow] CHECK CONSTRAINT [FK_WorkFlow_Fund]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_WorkFlow_WorkFlowStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[WorkFlow]'))
ALTER TABLE [dbo].[WorkFlow]  WITH CHECK ADD  CONSTRAINT [FK_WorkFlow_WorkFlowStatus] FOREIGN KEY([WorkFlowStatusId])
REFERENCES [dbo].[WorkFlowStatus] ([WorkFlowStatusId])
GO
ALTER TABLE [dbo].[WorkFlow] CHECK CONSTRAINT [FK_WorkFlow_WorkFlowStatus]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_WorkFlowHistory_WorkFlow]') AND parent_object_id = OBJECT_ID(N'[dbo].[WorkFlowHistory]'))
ALTER TABLE [dbo].[WorkFlowHistory]  WITH CHECK ADD  CONSTRAINT [FK_WorkFlowHistory_WorkFlow] FOREIGN KEY([WorkFlowId])
REFERENCES [dbo].[WorkFlow] ([WorkFlowId])
GO
ALTER TABLE [dbo].[WorkFlowHistory] CHECK CONSTRAINT [FK_WorkFlowHistory_WorkFlow]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_WorkFlowHistory_WorkFlowStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[WorkFlowHistory]'))
ALTER TABLE [dbo].[WorkFlowHistory]  WITH CHECK ADD  CONSTRAINT [FK_WorkFlowHistory_WorkFlowStatus] FOREIGN KEY([WorkFlowStatusId])
REFERENCES [dbo].[WorkFlowStatus] ([WorkFlowStatusId])
GO
ALTER TABLE [dbo].[WorkFlowHistory] CHECK CONSTRAINT [FK_WorkFlowHistory_WorkFlowStatus]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProcessLog_Rate]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProcessLog]'))
ALTER TABLE [dbo].[ProcessLog]  WITH CHECK ADD  CONSTRAINT [FK_ProcessLog_Rate] FOREIGN KEY([RateId])
REFERENCES [dbo].[Rate] ([RateId])
GO
ALTER TABLE [dbo].[ProcessLog] CHECK CONSTRAINT [FK_ProcessLog_Rate]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProcessLog_WorkFlow]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProcessLog]'))
ALTER TABLE [dbo].[ProcessLog]  WITH CHECK ADD  CONSTRAINT [FK_ProcessLog_WorkFlow] FOREIGN KEY([WorkFlowId])
REFERENCES [dbo].[WorkFlow] ([WorkFlowId])
GO
ALTER TABLE [dbo].[ProcessLog] CHECK CONSTRAINT [FK_ProcessLog_WorkFlow]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Schedule_Task]') AND parent_object_id = OBJECT_ID(N'[dbo].[Schedule]'))
ALTER TABLE [dbo].[Schedule]  WITH CHECK ADD  CONSTRAINT [FK_Schedule_Task] FOREIGN KEY([TaskId])
REFERENCES [dbo].[Task] ([TaskId])
GO
ALTER TABLE [dbo].[Schedule] CHECK CONSTRAINT [FK_Schedule_Task]

