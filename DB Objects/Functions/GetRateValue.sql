SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRateValue]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetRateValue] 
(
	-- Add the parameters for the function here
	@inrate nvarchar(50)
)
RETURNS nvarchar(50)
AS
BEGIN
	-- neutralizes the rate value for blank, null, or zero values
	return isnull(cast(nullif(cast(isnull(nullif(isnull(@inrate, 0), ''''), 0) as decimal(18,6)), 0) as nvarchar(50)), '''')
END
' 
END

GRANT  EXECUTE  ON [dbo].[GetRateValue]  TO [CRSUser]

GO
