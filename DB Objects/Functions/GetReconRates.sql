
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetReconRates]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE FUNCTION [dbo].[GetReconRates]
(
	@FundId bigint,
	@ExportDate datetime
)
RETURNS TABLE
AS
RETURN
(
	select 
	i.InvestmentCode, tr.SetDate, tr.RateTypeCode,
	isnull(max(tr.ReconRateR2), '''') as ReconRate_R2, 
	isnull(max(tr.ReconRateSuperb), '''') as ReconRate_Superb, 
	isnull(max(tr.ReconRateCalibre), '''') as ReconRate_Calibre,
	case tr.RateTypeCode
		when ''INTERIM'' then ''''
		else isnull(r.R2Rate, '''')
	end as OrigRate_R2,
	isnull(r.SuperbRate, '''') as OrigRate_Superb,
	isnull(r.CalibreRate, '''') as OrigRate_Calibre,
	case dbo.GetRateValue(max(tr.ReconRateR2))
		when
			case tr.RateTypeCode
				when ''INTERIM'' then ''''
				else dbo.GetRateValue(r.R2Rate)
			end
			then ''Yes''
		else ''No''
	end as Recon_R2,
	case dbo.GetRateValue(max(tr.ReconRateSuperb))
		when dbo.GetRateValue(r.SuperbRate) then ''Yes''
		else ''No''
	end as Recon_Superb,
	case dbo.GetRateValue(max(tr.ReconRateCalibre))
		when dbo.GetRateValue(r.CalibreRate) then ''Yes''
		else ''No''
	end as Recon_Calibre
	from
		dbo.ReconTempRates tr
	right outer join dbo.Investment i
		on i.InvestmentCode=tr.InvestmentCode
	left join dbo.Rate r
		on (r.SetDate=tr.SetDate and r.InvestmentId=i.InvestmentId)
	where
		i.FundId=@FundId and (tr.RowId is null or datediff(day, tr.ExportDate, @ExportDate) = 0)
	group by i.InvestmentCode, tr.SetDate, tr.RateTypeCode, r.R2Rate, r.SuperbRate, r.CalibreRate
	having tr.SetDate is not null

	UNION ALL

	select 
	i.InvestmentCode, r.SetDate, tr.RateTypeCode,
	isnull(max(tr.ReconRateR2), '''') as ReconRate_R2, 
	isnull(max(tr.ReconRateSuperb), '''') as ReconRate_Superb, 
	isnull(max(tr.ReconRateCalibre), '''') as ReconRate_Calibre,
	case tr.RateTypeCode
		when ''INTERIM'' then ''''
		else isnull(r.R2Rate, '''')
	end as OrigRate_R2,
	isnull(r.SuperbRate, '''') as OrigRate_Superb,
	isnull(r.CalibreRate, '''') as OrigRate_Calibre,
	case dbo.GetRateValue(max(tr.ReconRateR2))
		when
			case tr.RateTypeCode
				when ''INTERIM'' then ''''
				else dbo.GetRateValue(r.R2Rate)
			end
			then ''Yes''
		else ''No''
	end as Recon_R2,
	case dbo.GetRateValue(max(tr.ReconRateSuperb))
		when dbo.GetRateValue(r.SuperbRate) then ''Yes''
		else ''No''
	end as Recon_Superb,
	case dbo.GetRateValue(max(tr.ReconRateCalibre))
		when dbo.GetRateValue(r.CalibreRate) then ''Yes''
		else ''No''
	end as Recon_Calibre
	from
		dbo.ReconTempRates tr
	right join dbo.Rate r
		on r.SetDate=tr.SetDate
	inner join dbo.Investment i
		on r.InvestmentId=i.InvestmentId
	inner join dbo.WorkFlow wf
		on r.WorkFlowId=wf.WorkFlowId
	where 
		i.FundId=@FundId and i.InvestmentCode=tr.InvestmentCode and  datediff(day, wf.DateExported, @ExportDate) = 0
	group by i.InvestmentCode, r.SetDate, tr.RateTypeCode, r.R2Rate, r.SuperbRate, r.CalibreRate
	having r.SetDate is not null
)
' 
END

GRANT  EXECUTE  ON [dbo].[GetReconRates]  TO [CRSUser]

GO
