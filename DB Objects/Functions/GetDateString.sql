
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetDateString]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetDateString] 
(
	-- Add the parameters for the function here
	@indate datetime
)
RETURNS nvarchar(10)
AS
BEGIN

	declare @outdate nvarchar(10)

	set @outdate = cast(datepart(yyyy, @indate) as nvarchar(4)) + ''-'' + 
				cast(datepart(mm, @indate) as nvarchar(2)) + ''-'' + 
				cast(datepart(dd, @indate) as nvarchar(2))

	return @outdate
END
' 
END

GRANT  EXECUTE  ON [dbo].[GetDateString]  TO [CRSUser]

GO
