
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetExternalSystemCode]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetExternalSystemCode] 
(
	-- Add the parameters for the function here
	@systemId smallint
)
RETURNS nvarchar(20)
AS
BEGIN

	declare @systemCode nvarchar(20)

	select @systemCode=
	case @systemId
		when 1 then ''R2''
		when 2 then ''Superb''
		when 3 then ''Calibre''
	end

	return @systemCode
END


' 
END

GRANT  EXECUTE  ON [dbo].[GetExternalSystemCode]  TO [CRSUser]

GO
