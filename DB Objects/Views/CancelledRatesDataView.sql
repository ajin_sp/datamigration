SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CancelledRatesDataView]'))
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[CancelledRatesDataView]
AS
SELECT
     r.RateId, 
     f.R2FundId fundCltArrId,
     t.RateTypeInterfaceCode tranTypeRateType,
     i.InvestmentCode,
     r.StartDate startDate,
     r.EndDate endDate,
     w.DateTime authorisedDate,
     w.AssignTo authorisedBy,
     r.R2Rate annualRate,
     r.DateModified cancelledDate,
     r.ModifiedBy cancelledBy
	FROM Rate r, RateType t, Investment i, WorkFlow w, Fund f
	WHERE    
    w.FundId = f.FundId and
	w.WorkFlowId = r.WorkFlowId and	
    r.RateTypeId = t.RateTypeId and    
	r.InvestmentId = i.InvestmentId and
    r.Cancelled = 1
' 
