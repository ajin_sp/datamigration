SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ExportableRatesDataView]'))
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[ExportableRatesDataView]
AS
SELECT     TOP (100) PERCENT * FROM
(
	SELECT r.RateId AS creditingRateId, r.WorkFlowId as RateWorkFlowId, 
						  f.R2FundId AS fundCltArrId, rt.RateTypeInterfaceCode AS rateTypeCode, 
						  i.InvestmentCode AS investmentOptionCode, r.CreatedBy AS createdBy, ISNULL
							  ((SELECT     TOP (1) wh.AssignTo
								  FROM         dbo.WorkFlowHistory AS wh INNER JOIN
														dbo.WorkFlowStatus AS ws ON wh.WorkFlowStatusId = ws.WorkFlowStatusId
								  WHERE     (wh.WorkFlowId = r.WorkFlowId) AND (ws.WorkFlowStatusCode = ''Verified'')
								  ORDER BY wh.DateTime DESC), r.CreatedBy) AS verifiedBy, ISNULL
							  ((SELECT     TOP (1) wh.AssignTo
								  FROM         dbo.WorkFlowHistory AS wh INNER JOIN
														dbo.WorkFlowStatus AS ws ON wh.WorkFlowStatusId = ws.WorkFlowStatusId
								  WHERE     (wh.WorkFlowId = r.WorkFlowId) AND (ws.WorkFlowStatusCode = ''Approved'')
								  ORDER BY wh.DateTime DESC), r.CreatedBy) AS authorisedBy, 
						  (CASE WHEN r.R2Rate is null or r.R2Rate='''' THEN '''' ELSE CAST(r.R2Rate AS nvarchar(50)) END) AS rateR2, 
						  (CASE WHEN r.SuperbRate is null or r.SuperbRate='''' THEN '''' ELSE CAST(r.SuperbRate AS nvarchar(50)) END) AS rateSuperb,
						  (CASE WHEN r.CalibreRate is null or r.CalibreRate='''' THEN '''' ELSE CAST(r.CalibreRate AS nvarchar(50)) END) AS rateCalibre, 
						  i.SuperbDeclaredCode AS declaredCodeSuperb,
						  i.SuperbInterimCode AS interimCodeSuperb,
						  i.CalibreDeclaredCode AS declaredCodeCalibre,
						  i.CalibreInterimCode AS interimCodeCalibre,
						  (CASE rp.RatePeriodCode WHEN ''pd'' THEN ''true'' ELSE ''false'' END) AS isDailyRate, 
						  r.SetDate As effectiveDate, r.StartDate AS startDate, r.EndDate AS endDate, r.DateCreated AS createdDate, ISNULL
							  ((SELECT     TOP (1) wh.DateTime
								  FROM         dbo.WorkFlowHistory AS wh INNER JOIN
														dbo.WorkFlowStatus AS ws ON wh.WorkFlowStatusId = ws.WorkFlowStatusId
								  WHERE     (wh.WorkFlowId = r.WorkFlowId) AND (ws.WorkFlowStatusCode = ''Verified'')
								  ORDER BY wh.DateTime DESC), r.DateCreated) AS verifiedDate, ISNULL
							  ((SELECT     TOP (1) wh.DateTime
								  FROM         dbo.WorkFlowHistory AS wh INNER JOIN
														dbo.WorkFlowStatus AS ws ON wh.WorkFlowStatusId = ws.WorkFlowStatusId
								  WHERE     (wh.WorkFlowId = r.WorkFlowId) AND (ws.WorkFlowStatusCode = ''Approved'')
								  ORDER BY wh.DateTime DESC), r.DateCreated) AS authorisedDate
	FROM         dbo.Rate AS r INNER JOIN
						  dbo.Investment AS i ON r.InvestmentId = i.InvestmentId INNER JOIN
						  dbo.Fund AS f ON i.FundId = f.FundId INNER JOIN
						  dbo.RateType AS rt ON r.RateTypeId = rt.RateTypeId INNER JOIN
						  dbo.RatePeriod AS rp ON r.RatePeriodId = rp.RatePeriodId
	WHERE     (r.WorkFlowId IN
							  (SELECT     w.WorkFlowId
								FROM          dbo.WorkFlow AS w INNER JOIN
													   dbo.WorkFlowStatus AS ws ON w.WorkFlowStatusId = ws.WorkFlowStatusId
								WHERE      (ws.WorkFlowStatusCode = ''Approved'')))
				AND r.Cancelled=0 AND r.Deleted=0
) subq
WHERE 
	(rateR2 is not null and rateR2 <> '''')
	OR (rateSuperb is not null and rateSuperb <> '''')
	OR (rateCalibre is not null and rateCalibre <> '''')
ORDER BY fundCltArrId, startDate, RateWorkFlowId
' 
