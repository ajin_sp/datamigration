
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ClearReconTempRates]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ClearReconTempRates] 
	@SystemId smallint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	delete from ReconTempRates 
	where ExternalSystemId=@SystemId and 
	datediff(day, DateCreated, getdate())=0

	Select 1 as ReturnValue -- return dummy value
END


' 
END

GRANT  EXECUTE  ON [dbo].[ClearReconTempRates]  TO [CRSUser]

GO
