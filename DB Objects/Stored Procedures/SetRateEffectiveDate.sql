
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SetRateEffectiveDate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


CREATE PROCEDURE [dbo].[SetRateEffectiveDate] 	
	@CreditingRateId bigint,
	@EffectiveDate datetime,
	@ResultXml xml output,
	@ErrorMessage nvarchar(max) output
AS
BEGIN
	Declare @Err int

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRY					
	        UPDATE Rate
	        SET SetDate = @EffectiveDate
	        WHERE RateId = @CreditingRateId and
	        Cancelled = 0

			If (@@RowCount = ''0'')
				Begin
					Set @ResultXml = null
					Set @ErrorMessage = ''No rate is found for the specified criteria''
				End
			Else
				Begin
					SET @ResultXml = ''<programIO><programUnitName>CRS.SetRateEffectiveDate</programUnitName>'' + (
					SELECT  [fundCltArrId]
					  ,[tranTypeRateType]
					  ,[InvestmentCode]
					  ,[startDate]
					  ,[endDate]
					  ,[authorisedDate]
					  ,[authorisedBy]
					  ,[annualRate]
					  ,'''' [cancelledDate]
					  ,'''' [cancelledBy]
					FROM   ApprovedRatesDataView as creditingRate
					WHERE
					RateId = @CreditingRateId
					ORDER BY
					StartDate
					FOR XML AUTO, ELEMENTS
					) + ''</programIO>''

					Set @ErrorMessage = null
				End			       

	END TRY
		BEGIN CATCH
			Set @Err = @@ERROR 
			Set @ResultXml = null
			Set @ErrorMessage = ''Error occured while invoking SetRateEffectiveDate, transaction is rollbacked. Error code is '' + CAST(@Err as nvarchar(100))				
		END CATCH

    
END





' 
END

GRANT  EXECUTE  ON [dbo].[SetRateEffectiveDate]  TO [CRSUser]

GO
