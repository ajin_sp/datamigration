
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetDefaultInvestmentCode]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[GetDefaultInvestmentCode]
    @R2FundId bigint,
	@ResultXml xml output,
    @ErrorMessage nvarchar(max) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Set @ResultXml =
	(
	SELECT InvestmentCode
    FROM Investment, Fund f
    WHERE 
    Investment.FundId = f.FundId and
    Investment.IsDefault = 1 and
    f.R2FundId = @R2FundId
	FOR XML AUTO, TYPE, ELEMENTS
    )

--	If (@@RowCount = ''0'')
--		Begin
--			Set @ResultXml = null
--			Set @ErrorMessage = ''No rate is found for the specified criteria''
--		End
--	Else
--		Begin
--			Set @ErrorMessage = null
--		End	
END

' 
END

GRANT  EXECUTE  ON [dbo].[GetDefaultInvestmentCode]  TO [CRSUser]

GO
