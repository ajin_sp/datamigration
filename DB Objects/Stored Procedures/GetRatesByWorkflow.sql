
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRatesByWorkflow]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetRatesByWorkflow] 
	-- Add the parameters for the stored procedure here
	@TargetWorkflowId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [RateId]
      ,i.InvestmentDescription [InvestmentDescription]
      ,p.RatePeriodDescription [RatePeriodDescription]
      ,t.RateTypeDescription [RateTypeCode]
      ,[StartDate]
      ,[EndDate]
      ,[SetDate]
      ,[CreditingRate]                  
      ,[R2Rate]
      ,[SuperbRate]
      ,[CalibreRate]
      ,r.[DateCreated] [DateCreated]
      ,r.[CreatedBy] [CreatedBy]
      ,r.[DateModified] [DateModified]
      ,r.[ModifiedBy] [ModifiedBy]
      ,[Cancelled]
  FROM [Rate] r, [Investment] i, RateType t, RatePeriod p
  WHERE 
  r.WorkflowId = @TargetWorkflowId and
  i.InvestmentId = r.InvestmentId and
  t.RateTypeId = r.RateTypeId and
  p.RatePeriodId = r.RatePeriodId
  ORDER BY r.RateTypeId, r.StartDate, r.EndDate, r.SetDate  
END





' 
END

GRANT  EXECUTE  ON [dbo].[GetRatesByWorkflow]  TO [CRSUser]

GO
