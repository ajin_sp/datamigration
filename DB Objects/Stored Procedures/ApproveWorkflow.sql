
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ApproveWorkflow]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ApproveWorkflow] 
	@WorkFlowId bigint,
    @SetDate datetime,
    @ApprovedBy nvarchar(10)
AS
BEGIN
	Declare @WorkFlowStatusId bigint
	
	BEGIN TRY
		BEGIN TRANSACTION
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
        SELECT @WorkFlowStatusId = WorkFlowStatusId
		FROM [dbo].[WorkFlowStatus]
		WHERE WorkFlowStatusCode = ''Approved''

		UPDATE Rate
		SET SetDate = @SetDate
		WHERE WorkFlowId = @WorkFlowId		

		UPDATE [WorkFlow]
		SET  [WorkFlowStatusId] = @WorkFlowStatusId,
			 [AssignTo] = @ApprovedBy,
			 [DateTime] = getdate()
		WHERE WorkFlowId = @WorkFlowId
		
		COMMIT TRANSACTION

		-- Return the count of the rates
		select count(r.RateId) as RateCount
		FROM Rate r, WorkFlow w
		WHERE w.WorkFlowId = @WorkFlowId and
        w.WorkFlowStatusId = @WorkFlowStatusId and
        w.AssignTo = @ApprovedBy and
        r.WorkFlowId = w.WorkFlowId and
		r.SetDate = @SetDate

	END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
		END CATCH	
END





' 
END

GRANT  EXECUTE  ON [dbo].[ApproveWorkflow]  TO [CRSUser]

GO
