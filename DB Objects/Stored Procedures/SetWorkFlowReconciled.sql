
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SetWorkFlowReconciled]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SetWorkFlowReconciled] 
	@SystemId smallint,
	@FundId bigint,
	@ExportDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @sqlquery nvarchar(max)
	declare @systemCode nvarchar(20)
	declare @ExportedWorkFlowStatusId bigint
	declare @DateExportedVal nvarchar(10)

	select @systemCode = dbo.GetExternalSystemCode(@SystemId)

	-- Get the WorkFlowStatusId for ''Exported'' status
	select @ExportedWorkFlowStatusId = WorkFlowStatusId
	from dbo.WorkFlowStatus
	where WorkFlowStatusCode = ''Exported''

	set @DateExportedVal = dbo.GetDateString(@ExportDate)

	set @sqlquery = ''UPDATE dbo.WorkFlow 
			SET Reconciled'' + @systemCode + ''=1 
			WHERE FundId='' + cast(@FundId as nvarchar(10)) + '' and 
			WorkFlowStatusId='' + cast(@ExportedWorkFlowStatusId as nvarchar(10)) + '' and 
			DateExported is not null and datediff(day, DateExported, '''''' + @DateExportedVal + '''''')=0''

	--print @sqlquery
	exec(@sqlquery)

	Select 1 as ReturnValue -- return dummy value
END






' 
END

GRANT  EXECUTE  ON [dbo].[SetWorkFlowReconciled]  TO [CRSUser]

GO
