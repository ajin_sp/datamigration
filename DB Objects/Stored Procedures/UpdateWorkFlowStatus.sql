
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateWorkFlowStatus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateWorkFlowStatus] 
	@FundId bigint,
	@NewWorkFlowSequenceId bigint,
	@OldWorkFlowSequenceId bigint,
	@SetDateExported bit,
	@ExportDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @DateExportedVal nvarchar(10)
	declare @NewWorkFlowStatusId bigint
	declare @OldWorkFlowStatusId bigint

	select @NewWorkFlowStatusId=WorkFlowStatusId
	from dbo.WorkFlowStatus where WorkFlowSequenceId=@NewWorkFlowSequenceId

	select @OldWorkFlowStatusId=WorkFlowStatusId
	from dbo.WorkFlowStatus where WorkFlowSequenceId=@OldWorkFlowSequenceId

	if (@SetDateExported = 1)
	begin
		update dbo.WorkFlow
		set DateExported=@ExportDate
		where WorkFlowStatusId=@OldWorkFlowStatusId
		and FundId=@FundId
		and ReconciledR2=0
		and ReconciledSuperb=0
		and ReconciledCalibre=0
	end

	set @DateExportedVal = dbo.GetDateString(@ExportDate)

    -- Insert statements for procedure here
	declare @sqlquery nvarchar(max)
	set @sqlquery = ''UPDATE dbo.WorkFlow 
			SET WorkFlowStatusId='' + cast(@NewWorkFlowStatusId as nvarchar(20)) + '' 
			WHERE FundId='' + cast(@FundId as nvarchar(20)) + '' and 
			WorkFlowStatusId='' + cast(@OldWorkFlowStatusId as nvarchar(20)) + '' and
			(DateExported is null or datediff(day, DateExported, '''''' + @DateExportedVal + '''''')=0)''

	print @sqlquery
	exec(@sqlquery)

	Select 1 as ReturnValue -- return dummy value
END




' 
END

GRANT  EXECUTE  ON [dbo].[UpdateWorkFlowStatus]  TO [CRSUser]

GO
