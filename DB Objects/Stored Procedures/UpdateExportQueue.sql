
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateExportQueue]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UpdateExportQueue]
	@SequenceId bigint,
	@StatusId tinyint,
	@ErrorMessage nvarchar(MAX),
	@ErrorRateId bigint,
	@YTDRatesZip varbinary(MAX),
	@ActionResult tinyint OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- Returns operation result Id (0=failed, 1=success)

BEGIN TRY

	declare @seqcheck tinyint

	select @seqcheck=count(SeqId) from dbo.ExportQueue where SeqId=@SequenceId 

	if (@seqcheck <> 1)
		begin
			INSERT INTO dbo.ProcessLog
					   (ProcessModule, LogMessage, DateLogged)
				 VALUES
					   (''DB.USP.UpdateExportQueue'',
						''Invalid sequence Id (#'' + cast(@SequenceId as nvarchar(20)) + ''). Not found or not unique'',
						getdate())

			Select @ActionResult = 0
		end
	else
		begin
			-- StatusId values: 1=new, 2=partial, 3=completed, 9=error
			if (@StatusId = 3 AND (@YTDRatesZip is null OR DATALENGTH(@YTDRatesZip) < 10))
				begin
					INSERT INTO dbo.ProcessLog
							   (ProcessModule, LogMessage, DateLogged)
						 VALUES
							   (''DB.USP.UpdateExportQueue'',
								''Invalid invocation. Interface tried to mark export (#'' + cast(@SequenceId as nvarchar(20)) + '') as completed without required data (YTD rates)'',
								getdate())

					Select @ActionResult = 0
				end
			else
				begin
					update dbo.ExportQueue
					set
						StatusId=@StatusId,
						ErrorMessage=@ErrorMessage,
						ErrorRateId=@ErrorRateId,
						YTDRatesZip=@YTDRatesZip,
						DateLastUpdated=getdate()
					where SeqId=@SequenceId

					Select @ActionResult = 1
				end
		end

END TRY
BEGIN CATCH
	INSERT INTO dbo.ProcessLog
			   (ProcessModule, LogMessage, DateLogged)
		 VALUES
			   (''DB.USP.UpdateExportQueue'',
				''Error on interface invocation for export (#'' + cast(@SequenceId as nvarchar(20)) + ''): '' + @@ERROR,
				getdate())

	Select @ActionResult = 0
END CATCH

END

' 
END

GRANT  EXECUTE  ON [dbo].[UpdateExportQueue]  TO [CRSUser]

GO
