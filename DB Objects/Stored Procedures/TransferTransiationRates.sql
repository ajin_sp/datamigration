
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransferTransiationRates]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TransferTransiationRates]
(
	@FundId bigint,
	@SessionId nvarchar(max),
	@AutoApproveWorkflow bit
)
AS
Declare @RateCount int
Declare @WorkFlowStatusId bigint
Declare @WorkFlowId bigint
Declare @CreatedBy nvarchar(10)
BEGIN

BEGIN TRY
	BEGIN TRANSACTION
		SET NOCOUNT ON;

		declare @WorkFlowStatusCode nvarchar(10)

		-- Set Workflow status code
		set @WorkFlowStatusCode = ''Created''
		if (@AutoApproveWorkflow = 1)
		begin
			set @WorkFlowStatusCode = ''Approved''
		end

		-- Get count of rate records
		select @RateCount =  count(r.rateid)
		from dbo.TransitionRate tr, dbo.Rate r
		where 
		cast(tr.setdate as nvarchar(11)) = cast(r.setdate as nvarchar(11)) and
		cast(tr.startdate as nvarchar(11)) = cast(r.startdate as nvarchar(11)) and
		tr.investmentid = r.investmentid

		-- Get the WorkFlowStatusId for ''Created'' status
		select @WorkFlowStatusId = WorkFlowStatusId
		from [dbo].[WorkFlowStatus]
		where WorkFlowStatusCode = @WorkFlowStatusCode

		-- Get the userid of the user who entered the rates
		SELECT TOP 1 @CreatedBy =  CreatedBy
		FROM [dbo].[TransitionRate]
		WHERE Guid = @SessionId

		PRINT @CreatedBy

		if (@RateCount = 0)
		Begin	
			-- Create a new workflow record
			INSERT INTO [dbo].[WorkFlow]
			(
				[AssignTo],
				[WorkFlowStatusId],
				[FundId]
			)
			SELECT @CreatedBy As [AssignTo], @WorkFlowStatusId As [WorkFlowStatusId], @FundId As [FundId]
			
			SET @WorkFlowId = SCOPE_IDENTITY()

			-- Create a new workflowhistory record
			INSERT INTO [dbo].[WorkFlowHistory]
			   (
				 [WorkFlowId]
				,[WorkFlowStatusId]
				,[AssignTo]
				,[DateTime]
			   )
			SELECT 
				 @WorkFlowId
				,@WorkFlowStatusId
				,@CreatedBy
				,getdate()

			-- Transfer rates from TransitionRate to Rate
			INSERT INTO [dbo].[Rate]
			(
				[RateTypeId]
				,[StartDate]
				,[EndDate]
				,[SetDate]
				,[CreditingRate]
				,[ConvertedRate]
				,[PensionRate]
				,[RatePeriodId]
				,[MonthlyUseRate]
				,[R2Rate]
				,[SuperbRate]
				,[CalibreRate]		
				,[InvestmentId]
				,[DateCreated]
				,[CreatedBy]
				,[DateModified]
				,[ModifiedBy]
				,[FundRuleId]
				,[WorkFlowId]
                ,[Cancelled]
                ,[Deleted] 
			)
			SELECT 
			  [TransitionRateTypeId]
			  ,[StartDate]
			  ,[EndDate]
			  ,[SetDate]
			  ,[CreditingTransitionRate]
			  ,[ConvertedTransitionRate]
			  ,[PensionTransitionRate]
			  ,[TransitionRatePeriodId]
			  ,[TransitionMonthlyUseRate]
			  ,[R2TransitionRate]
			  ,[SuperbTransitionRate]
			  ,[CalibreTransitionRate]      
			  ,[InvestmentId]
			  ,[DateCreated]
			  ,[CreatedBy]
			  ,[DateModified]
			  ,[ModifiedBy]
			  ,[FundRuleId]
			  ,@WorkFlowId
              ,[Cancelled]
              ,0
			FROM [dbo].[TransitionRate]
			WHERE Guid = @SessionId
		End

		-- Delete the records in TransitionRate for the given GUID
		DELETE
		FROM dbo.TransitionRate
		WHERE Guid = @SessionId

		COMMIT TRANSACTION

		-- Return the count of the rates
		select @RateCount as RateCount

	END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
		END CATCH	
END



























' 
END

GRANT  EXECUTE  ON [dbo].[TransferTransiationRates]  TO [CRSUser]

GO
