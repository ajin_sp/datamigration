
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InvokeProgramUnit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InvokeProgramUnit] 
	-- Add the parameters for the stored procedure here
	@ProgramUnit nvarchar(max),
	@XmlClobIn xml,
	@XmlClobOut nvarchar(max) output,
	@ErrorMessage nvarchar(max) output 
AS
BEGIN	
	Declare @NodeName SYSNAME
	Declare @MethodFound bit
	Declare @MethodParameterCount int
	Declare @MethodParameterFound bit
	Declare @SeqId nvarchar(max)
    Declare @Err int
	Declare @XmlOut xml

	Declare @CreditingRateId bigint
	Declare @FundCltArrId bigint
	Declare @RateTypeCode nvarchar(15)
    Declare @InvestmentOptionCode nvarchar(50)
	Declare @StartDate datetime    
	Declare @EndDate datetime
	Declare @EffectiveDate datetime
	Declare @CancelledDate datetime
    Declare @CancelledBy nvarchar(10)


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--	BEGIN TRY
		SET @NodeName = ''param''
		Set @MethodFound = 0
		Set @MethodParameterFound = 0
		Select @SeqId = newId()
		exec GetXmlMethodParameterValues @XmlClobIn, @NodeName, @SeqId 
		
		select @MethodParameterCount = count(*) from XmlMethodParameters

		if (upper(@ProgramUnit) = ''GETDEFAULTINVESTMENTCODE'')
			begin
				Set @MethodFound = 1
				if (@MethodParameterCount = 1)
					begin					
						Set @MethodParameterFound = 1
						
						select @fundCltArrId = XmlValue
						from XmlMethodParameters
						where upper(XmlNodeName) = ''FUNDCLTARRID''					

						exec GetDefaultInvestmentCode @fundCltArrId, @XmlOut output, @ErrorMessage output
					end
			end
			
		if (upper(@ProgramUnit) = ''GETRATESSTARTINGON'')
			begin
				Set @MethodFound = 1
				if (@MethodParameterCount = 4)
					begin					
						Set @MethodParameterFound = 1						
						
						select @FundCltArrId = XmlValue
						from XmlMethodParameters
						where upper(XmlNodeName) = ''FUNDCLTARRID''
						
						select @RateTypeCode = XmlValue
						from XmlMethodParameters
						where upper(XmlNodeName) = ''RATETYPE''

					    select @InvestmentOptionCode = XmlValue
						from XmlMethodParameters
						where upper(XmlNodeName) = ''INVESTMENTOPTION''
						
						select @StartDate = XmlValue
						from XmlMethodParameters
						where upper(XmlNodeName) = ''STARTDATE''						

						exec GetRatesStartingOn @FundCltArrId, @RateTypeCode, @InvestmentOptionCode, @StartDate, @XmlOut output, @ErrorMessage output
					end
			end

		if (upper(@ProgramUnit) = ''GETLATESTRATEBEFORESTARTDATE'')
			begin
				Set @MethodFound = 1
				if (@MethodParameterCount = 4)
					begin					
						Set @MethodParameterFound = 1						
						
						select @FundCltArrId = XmlValue
						from XmlMethodParameters
						where upper(XmlNodeName) = ''FUNDCLTARRID''
						
						select @RateTypeCode = XmlValue
						from XmlMethodParameters
						where upper(XmlNodeName) = ''RATETYPE''

					    select @InvestmentOptionCode = XmlValue
						from XmlMethodParameters
						where upper(XmlNodeName) = ''INVESTMENTOPTION''
						
						select @StartDate = XmlValue
						from XmlMethodParameters
						where upper(XmlNodeName) = ''STARTDATE''						

						exec GetLatestRateBeforeStartDate @FundCltArrId, @RateTypeCode, @InvestmentOptionCode, @StartDate, @XmlOut output, @ErrorMessage output
					end
			end
			
		if (upper(@ProgramUnit) = ''ALTERRATEENDDATE'')
			begin
				Set @MethodFound = 1
				if (@MethodParameterCount = 2)
					begin					
						Set @MethodParameterFound = 1						

						select @CreditingRateId = XmlValue
						from XmlMethodParameters
						where upper(XmlNodeName) = ''CREDITINGRATEID''
						
						select @EndDate = XmlValue
						from XmlMethodParameters
						where upper(XmlNodeName) = ''ENDDATE''

						exec AlterRateEndDate @CreditingRateId, @EndDate, @XmlOut output, @ErrorMessage output
					end
			end
			
		if (upper(@ProgramUnit) = ''CANCELRATE'')
			begin
				Set @MethodFound = 1
				Print @MethodParameterCount
				if (@MethodParameterCount = 3)
					begin
						Set @MethodParameterFound = 1						

						select @CreditingRateId = XmlValue
						from XmlMethodParameters
						where upper(XmlNodeName) = ''CREDITINGRATEID''

						select @CancelledDate = XmlValue
						from XmlMethodParameters
						where upper(XmlNodeName) = ''CANCELLEDDATE''
						
						select @CancelledBy = XmlValue
						from XmlMethodParameters
						where upper(XmlNodeName) = ''CANCELLEDBY''
						
						exec CancelRate @CreditingRateId, @CancelledDate, @CancelledBy, @XmlOut output, @ErrorMessage output					
						print ''1 '' + @ErrorMessage
					end
			end

		if (upper(@ProgramUnit) = ''SETEFFECTIVEDATE'')
			begin
                Set @MethodFound = 1
				if (@MethodParameterCount = 2)
					begin
						Set @MethodParameterFound = 1						
						
						select @CreditingRateId = XmlValue
						from XmlMethodParameters
						where upper(XmlNodeName) = ''CREDITINGRATEID''
						
						select @EffectiveDate = XmlValue
						from XmlMethodParameters
						where upper(XmlNodeName) = ''EFFECTIVEDATE''

						exec SetRateEffectiveDate @CreditingRateId, @EffectiveDate, @XmlOut output, @ErrorMessage output
					end
			end


		if (@MethodFound = 0)
			begin
				Set @ErrorMessage = ''Specified method name '''''' + @ProgramUnit + '''''' is invalid'';
			end
	   else
			begin
				if (@MethodParameterFound = 0)
					begin
						Set @ErrorMessage = ''Number of parameters specified ('' + cast(@MethodParameterCount as nvarchar(10)) + '') for method '''''' + @ProgramUnit + '''''' is invalid'';
					end
			end				

		Delete XmlMethodParameters
		Where SeqId = @SeqId


--	END TRY
--		BEGIN CATCH
--			Set @Err = @@ERROR 
--			Set @ErrorMessage = ''Error occured while invoking '' + @ProgramUnit + '', transaction is rollbacked. Error code is '' + CAST(@Err as nvarchar(100))		
--			
--			Delete XmlMethodParameters
--			Where SeqId = @SeqId
--		END CATCH
	

	Set @XmlClobOut = cast(@XmlOut as nvarchar(max))
	
	select @XmlClobOut as ResultXml, @ErrorMessage as ErrorMessage
END

' 
END

GRANT  EXECUTE  ON [dbo].[InvokeProgramUnit]  TO [CRSUser]

GO
