
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetXmlMethodParameterValues]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'






CREATE PROCEDURE [dbo].[GetXmlMethodParameterValues] 	
	@XmlData xml,	
	@NodeName nvarchar(max),
	@SeqId nvarchar(max)	
AS
BEGIN
	Declare @column_name SYSNAME	

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	

    -- Insert statements for procedure here
	--TRUNCATE TABLE XmlMethodParameters

	INSERT INTO XmlMethodParameters	
    SELECT
		 @SeqId as SeqId, 		 
		 data.col.value(''@name'', ''nvarchar(max)'') XmlNodeName,
	     --data.col.value(''local-name(.)'', ''nvarchar(max)'') element_name,
	     data.col.value(''.'', ''nvarchar(max)'') XmlValue
    from @XmlData.nodes(''//params/*'') data(col)
    WHERE data.col.value(''local-name(.)'', ''nvarchar(max)'') = @NodeName	
END






' 
END

GRANT  EXECUTE  ON [dbo].[GetXmlMethodParameterValues]  TO [CRSUser]

GO
