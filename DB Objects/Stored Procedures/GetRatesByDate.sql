
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRatesByDate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[GetRatesByDate] 
	-- Add the parameters for the stored procedure here
	@StartDate datetime,
    @EndDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT [RateId]
      ,i.InvestmentDescription [InvestmentDescription]
      ,p.RatePeriodDescription [RatePeriodDescription]
      ,t.RateTypeDescription [RateTypeCode]
      ,[StartDate]
      ,[EndDate]
      ,[SetDate]
      ,[CreditingRate]                  
      ,[R2Rate]
      ,[SuperbRate]
      ,[CalibreRate]
      ,r.[DateCreated] [DateCreated]
      ,r.[CreatedBy] [CreatedBy]
      ,r.[DateModified] [DateModified]
      ,r.[ModifiedBy] [ModifiedBy]
      ,[Cancelled]
	  FROM [Rate] r, [Investment] i, RateType t, RatePeriod p
	  WHERE 
	  DateDiff(day, r.StartDate, @StartDate) = 0 and
      DateDiff(day, r.EndDate, @EndDate) = 0 and
	  i.InvestmentId = r.InvestmentId and
	  t.RateTypeId = r.RateTypeId and
	  p.RatePeriodId = r.RatePeriodId
	  ORDER BY r.StartDate, r.EndDate, r.SetDate, r.RateTypeId, r.RatePeriodId
END
' 
END

GRANT  EXECUTE  ON [dbo].[GetRatesByDate]  TO [CRSUser]

GO
