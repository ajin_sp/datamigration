
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetLatestRateBeforeStartDate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[GetLatestRateBeforeStartDate] 
	@R2FundId bigint,
	@RateTypeCode	nvarchar(15),
	@InvestmentOptionCode	nvarchar(50),
	@StartDate datetime,
    @ResultXml xml output,
    @ErrorMessage nvarchar(max) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Set @ErrorMessage = null
    Set @ResultXml = ''<programIO><programUnitName>CRS.GetLatestRateBeforeStartDate</programUnitName>'' + (
	SELECT  [fundCltArrId]
      ,[tranTypeRateType]
      ,[InvestmentCode]
      ,[startDate]
      ,[endDate]
      ,[authorisedDate]
      ,[authorisedBy]
      ,[annualRate]
      ,'''' [cancelledDate]
      ,'''' [cancelledBy]
    FROM   ApprovedRatesDataView as creditingRate
	where startdate =
	(
		select max(startdate)
		from ApprovedRatesDataView
		where 
		fundCltArrId = @R2FundId and
		datediff(day,startDate, @StartDate) <= 0 and
		InvestmentCode  = @InvestmentOptionCode and
		tranTypeRateType = @RateTypeCode	
	) and
	fundCltArrId = @R2FundId and    
    InvestmentCode  = @InvestmentOptionCode and
	tranTypeRateType = @RateTypeCode
    FOR XML AUTO, ELEMENTS
	) + ''</programIO>''


--	If (@@RowCount = ''0'')
--		Begin
--			Set @ResultXml = null
--			Set @ErrorMessage = ''No rate is found for the specified criteria''
--		End
--	Else
--		Begin
--			Set @ErrorMessage = null
--		End
END

' 
END

GRANT  EXECUTE  ON [dbo].[GetLatestRateBeforeStartDate]  TO [CRSUser]

GO
