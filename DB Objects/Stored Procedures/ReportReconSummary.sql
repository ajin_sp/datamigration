
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportReconSummary]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ReportReconSummary] 
	@FundId bigint,
	@ExportDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select
	InvestmentCode,
	count(InvestmentCode) as NoOfEntries,
	count(ALL case ReconRate_R2 when '''' then null else ReconRate_R2 end) as TotalReconRates_R2,
	count(ALL case ReconRate_Superb when '''' then null else ReconRate_Superb end) as TotalReconRates_Superb,
	count(ALL case ReconRate_Calibre when '''' then null else ReconRate_Calibre end) as TotalReconRates_Calibre,
	count(ALL case OrigRate_R2 when '''' then null else OrigRate_R2 end) as TotalOrigRates_R2,
	count(ALL case OrigRate_Superb when '''' then null else OrigRate_Superb end) as TotalOrigRates_Superb,
	count(ALL case OrigRate_Calibre when '''' then null else OrigRate_Calibre end) as TotalOrigRates_Calibre,
	count(ALL case Recon_R2 when ''Yes'' then null else Recon_R2 end) as TotalRecon_R2,
	count(ALL case Recon_Superb when ''Yes'' then null else Recon_Superb end) as TotalRecon_Superb,
	count(ALL case Recon_Calibre when ''Yes'' then null else Recon_Calibre end) as TotalRecon_Calibre
	from
	(
		select * from dbo.GetReconRates(@FundId, @ExportDate)
	) as subq
	group by InvestmentCode
	order by InvestmentCode

END
' 
END

GRANT  EXECUTE  ON [dbo].[ReportReconSummary]  TO [CRSUser]

GO
