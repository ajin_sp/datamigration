
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SetAuthorisationPending]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SetAuthorisationPending] 
	@CreditingRateId bigint,
	@AuthorisationFlag nvarchar(1),
	@XmlOutput nvarchar(max),
	@ErrorMessage nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here	
END
' 
END

GRANT  EXECUTE  ON [dbo].[SetAuthorisationPending]  TO [CRSUser]

GO
