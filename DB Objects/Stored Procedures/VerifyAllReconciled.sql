
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VerifyAllReconciled]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[VerifyAllReconciled]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update dbo.SystemConfig 
	set ConfigValue=getdate() 
	where ConfigName=''LastSuccessfulReconcile''

--    declare @reconR2 bit
--	declare @reconSuperb bit
--	declare @reconCalibre bit
--	declare @expectedcount int
--	declare @tempstring nvarchar(max)
--	declare @currdateval nvarchar(10)
--	declare @doReconR2 smallint
--	declare @doReconSuperb smallint
--	declare @doReconCalibre smallint
--
--	set @doReconR2 = 0
--	set @doReconSuperb = 0
--	set @doReconCalibre = 0
--
--	set @tempstring = replace(@WorkFlowIDs, '','', '''')
--	set @expectedcount = (len(@WorkFlowIDs) - len(@tempstring)) + 1
--
--	select @currdateval = dbo.GetDateString(getdate())
--
--	select @doReconR2 = case ConfigValue when ''yes'' then 1 else 0 end from SystemConfig
--	where ConfigName=''ReconcileR2''
--
--	select @doReconSuperb = case ConfigValue when ''yes'' then 1 else 0 end from SystemConfig
--	where ConfigName=''ReconcileSuperb''
--
--	select @doReconCalibre = case ConfigValue when ''yes'' then 1 else 0 end from SystemConfig
--	where ConfigName=''ReconcileCalibre''
--
--	declare @sqlquery nvarchar(max)
--	set @sqlquery = ''update dbo.SystemConfig
--						set ConfigValue='''''' + @currdateval + '''''' 
--						where ConfigName=''''LastSuccessfulReconcile'''' and	(
--									select count(WorkFlowId) as cnt from dbo.WorkFlow
--									where WorkFlowId in ('' + @WorkFlowIDs + '') and 
--									ReconciledR2='' + cast(@doReconR2 as nvarchar(1)) + '' and
--									ReconciledSuperb='' + cast(@doReconSuperb as nvarchar(1)) + '' and
--									ReconciledCalibre='' + cast(@doReconCalibre as nvarchar(1)) + ''
--								) = '' + cast(@expectedcount as nvarchar(10)) + ''''
--
--	print @sqlquery
--	exec(@sqlquery)

	Select 1 as ReturnValue -- return dummy value
END

' 
END

GRANT  EXECUTE  ON [dbo].[VerifyAllReconciled]  TO [CRSUser]

GO
