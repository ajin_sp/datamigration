
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AlterRateEndDate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'




CREATE PROCEDURE [dbo].[AlterRateEndDate] 
	@CreditingRateId bigint,
	@EndDate datetime,
	@ResultXml xml output,
	@ErrorMessage nvarchar(max) output
AS
BEGIN
	Declare @Err int
	Declare @RateId bigint

	BEGIN TRY		
		-- SET NOCOUNT ON added to prevent extra result sets from
 	    -- interfering with SELECT statements.
	    SET NOCOUNT ON;

		SELECT @RateId = RateId
		FROM Rate r, WorkFlow w, WorkFlowStatus s
		WHERE
		r.RateId = @CreditingRateId and
		r.Cancelled = 0 and
		w.WorkFlowId = r.WorkFlowId and	
		s.WorkFlowStatusId = w.WorkFlowStatusId and
		s.WorkFlowStatusCode = ''Approved''

		if (@RateId > 0)
			Begin
				UPDATE Rate
				SET EndDate = @EndDate
				WHERE RateId = @RateId

				SET @ResultXml = ''<programIO><programUnitName>CRS.AlterRateEndDate</programUnitName>'' + (
				SELECT  [fundCltArrId]
				  ,[tranTypeRateType]
				  ,[InvestmentCode]
				  ,[startDate]
				  ,[endDate]
				  ,[authorisedDate]
				  ,[authorisedBy]
				  ,[annualRate]
				  ,'''' [cancelledDate]
				  ,'''' [cancelledBy]
				FROM   RatesDataView as creditingRate
				WHERE
				RateId = @CreditingRateId
				ORDER BY
				StartDate
				FOR XML AUTO, ELEMENTS
				) + ''</programIO>''
			
				Set @ErrorMessage = null				
			End
		Else
			Begin
				Set @ResultXml = null
				Set @ErrorMessage = ''Rate''''s end date cannot be altered''
			End
	END TRY
		BEGIN CATCH
			Set @Err = @@ERROR 
            Set @ResultXml = null
			Set @ErrorMessage = ''Error occured while invoking AlterRateEndDate, transaction is rollbacked. Error code is '' + CAST(@Err as nvarchar(100))					
		END CATCH	
END

' 
END

GRANT  EXECUTE  ON [dbo].[AlterRateEndDate]  TO [CRSUser]

GO
