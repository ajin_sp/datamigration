SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRates]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'










CREATE PROCEDURE [dbo].[GetRates] 
	-- Add the parameters for the stored procedure here
	@UserId nvarchar(10),
	@FundId nvarchar(3),
	@InvestmentId nvarchar(3),
	@RateTypeId nvarchar(1),
	@RatePeriodId nvarchar(1),
	@StartDate nvarchar(10),
    @EndDate nvarchar(10),
	@SetDate nvarchar(10),
	@IncludeCancelled varchar(1)
AS
BEGIN
	Declare @Query nvarchar(max)
    Declare @QueryOrderBy nvarchar(max)
    declare @UserFundsCsv nvarchar(max)
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Set @Query = ''SELECT [RateId]
                  ,f.FundName [FundName]
				  ,i.InvestmentDescription [InvestmentDescription]
				  ,p.RatePeriodDescription [RatePeriodDescription]
				  ,t.RateTypeDescription [RateTypeCode]
				  ,[StartDate]
				  ,[EndDate]
				  ,[SetDate]
				  ,[CreditingRate]                  
				  ,[R2Rate]
				  ,[SuperbRate]
				  ,[CalibreRate]
				  ,r.[DateCreated] [DateCreated]
				  ,r.[CreatedBy] [CreatedBy]
				  ,r.[DateModified] [DateModified]
				  ,r.[ModifiedBy] [ModifiedBy]
				  ,[Cancelled]
				  FROM [Rate] r, [Investment] i, RateType t, RatePeriod p, Fund f
				  WHERE 				  
				  i.InvestmentId = r.InvestmentId and
                  f.FundId = i.FundId and
				  t.RateTypeId = r.RateTypeId and
				  p.RatePeriodId = r.RatePeriodId ''
    Set @QueryOrderBy    = ''ORDER BY i.FundId, r.StartDate, r.EndDate, r.SetDate, r.RateTypeId, r.RatePeriodId''

	-- get/set user funds for filteration
	Set @UserFundsCsv =
	(
		select '''''''' + f.FundName + '''''',''
		from Fund f, UserFunds uf
		where f.FundId=uf.FundId and upper(uf.Username)=upper(@UserId)
		order by f.FundName
		for xml path('''')
	) + ''''''-1''''''
	Set @Query = @Query + '' and f.FundName in ('' + @UserFundsCsv + '')''
	
	If (len(ltrim(rtrim(@FundId))) > 0)
		Begin			
			Set @Query = @Query + '' and i.FundId = '' + cast(cast(@FundId as bigint) as nvarchar(max))
		End

	If (@InvestmentId <> '''')
		Begin						
			Set @Query = @Query + '' and i.InvestmentId = '' + cast(cast(@InvestmentId as bigint) as nvarchar(max))			
		End

    If (@RateTypeId <> '''')
		Begin			
			Set @Query = @Query + '' and r.RateTypeId = '' + cast(cast(@RateTypeId as bigint) as nvarchar(max))			
		End

    If (@RatePeriodId <> '''')
		Begin			
			Set @Query = @Query + '' and r.RatePeriodId = '' + cast(cast(@RatePeriodId as bigint) as nvarchar(max))
		End

	If (@StartDate <> '''')
		Begin			
			Set @Query = @Query + '' and DateDiff(day,r.StartDate, cast(@StartDate as datetime)) = 0''
		End

	If (@EndDate <> '''')
		Begin			
			Set @Query = @Query + '' and DateDiff(day,r.EndDate, cast(@EndDate as datetime)) = 0''
		End

	If (@SetDate <> '''')
		Begin			
			Set @Query = @Query + '' and DateDiff(day,r.SetDate, cast(@SetDate as datetime)) = 0''
		End

	If (@IncludeCancelled <> '''')
		Begin			
			Set @Query = @Query + '' and r.Cancelled = '' + cast(cast(@IncludeCancelled as bit) as nvarchar(max))			
		End

		Set @Query = @Query + '' '' + @QueryOrderBy		
		
		execute(@Query)

		
END








' 
END

GRANT  EXECUTE  ON [dbo].[GetRates]  TO [CRSUser]

GO
