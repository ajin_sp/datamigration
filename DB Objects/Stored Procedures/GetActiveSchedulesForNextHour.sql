
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetActiveSchedulesForNextHour]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetActiveSchedulesForNextHour]
AS

declare @NextScheduleTime as datetime
declare @Year nvarchar(4)
declare @Month nvarchar(4)
declare @Day nvarchar(4)

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Get the current date and time and add 1 hour
	set @NextScheduleTime = getdate()
	set @NextScheduleTime = dateadd(hour, 1, @NextScheduleTime)

	-- Get the year, month and day based on the @NextScheduleTime value
    set @Year = datepart(year,@NextScheduleTime)
    set @Month = datepart(month,@NextScheduleTime)
    set @Day = datepart(day,@NextScheduleTime)

	-- Select the schedules based on the execution time for active schedules and active task
	select s.*
	from Schedule s, Task t
    where 
    s.TaskId = t.TaskId and
	t.Disabled = 0 and
	s.Disabled = 0 and
    datediff(mi, getdate(), cast(@Year + ''-'' + @Month + ''-'' + @Day + '' '' + ExecutionTime as datetime))between 0 and 60
    order by cast(@Year + ''-'' + @Month + ''-'' + @Day + '' '' + ExecutionTime as datetime)

END

' 
END

GRANT  EXECUTE  ON [dbo].[GetActiveSchedulesForNextHour]  TO [CRSUser]

GO
