
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRatesStartingOn]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'






CREATE PROCEDURE [dbo].[GetRatesStartingOn]
	@R2FundId bigint,
	@RateTypeCode	nvarchar(15),
	@InvestmentOptionCode	nvarchar(50),
	@StartDate datetime,
	@ResultXml xml output,
    @ErrorMessage nvarchar(max)	output
AS
BEGIN
	--Declare @RecordCount bigint
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	

--	Set @RecordCount = 0
--
--	SELECT  @RecordCount = count(*)
--    FROM   ApprovedRatesDataView as creditingRate
--	WHERE
--	fundCltArrId = @R2FundId and
--    InvestmentCode = @InvestmentOptionCode and
--	tranTypeRateType = @RateTypeCode and
--    DateDiff(day,StartDate, @StartDate) = 0
--
--	If (@RecordCount > 0)
--		Begin
			SET @ResultXml = ''<programIO><programUnitName>CRS.GetRatesStartingOn</programUnitName>'' + (
			SELECT  [fundCltArrId]
			  ,[tranTypeRateType]
			  ,[InvestmentCode]
			  ,[startDate]
			  ,[endDate]
			  ,[authorisedDate]
			  ,[authorisedBy]
			  ,[annualRate]
			  ,'''' [cancelledDate]
			  ,'''' [cancelledBy]
			FROM   ApprovedRatesDataView as creditingRate
			WHERE
			fundCltArrId = @R2FundId and
			InvestmentCode = @InvestmentOptionCode and
			tranTypeRateType = @RateTypeCode and
			DateDiff(day,StartDate, @StartDate) = 0
			ORDER BY
			StartDate
			FOR XML AUTO, ELEMENTS
			) + ''</programIO>''

			Set @ErrorMessage = null
--		End
--	Else
--		Begin
--			Set @ResultXml = null
--			Set @ErrorMessage = ''No rate is found for the specified criteria''
--		End

END


























' 
END

GRANT  EXECUTE  ON [dbo].[GetRatesStartingOn]  TO [CRSUser]

GO
