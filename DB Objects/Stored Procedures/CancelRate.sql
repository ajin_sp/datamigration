
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CancelRate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CancelRate]
	@CreditingRateId bigint,
	@CancelledDate datetime,
	@CancelledBy nvarchar(10),
    @ResultXml xml output,			 
	@ErrorMessage nvarchar(max) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @WorkflowId bigint

	Set @WorkflowId = 0;
	Set @ErrorMessage = '''';

	SELECT @WorkflowId = r.WorkFlowId
	FROM Rate r, WorkFlow w, WorkFlowStatus s
	WHERE
	r.RateId = @CreditingRateId and
	r.Cancelled = 0 and
	w.WorkflowId = r.WorkflowId and
	s.WorkFlowStatusId = w.WorkFlowStatusId and
	s.WorkFlowStatusCode = ''APPROVED'';

	If (@WorkflowId > 0)
		Begin
			UPDATE Rate
			SET DateModified = @CancelledDate,
			ModifiedBy = @CancelledBy,
			Cancelled = 1
			WHERE 
			RateId = @CreditingRateId and	 
			WorkflowId = @WorkflowId;
			
			SET @ResultXml = ''<programIO><programUnitName>CRS.CancelRate</programUnitName>'' + (
			SELECT  [fundCltArrId]
			  ,[tranTypeRateType]
			  ,[InvestmentCode]
			  ,[startDate]
			  ,[endDate]
			  ,[authorisedDate]
			  ,[authorisedBy]
			  ,[annualRate]
			  ,[cancelledDate]
			  ,[cancelledBy]
			FROM  CancelledRatesDataView  as creditingRate
			WHERE
			RateId = @CreditingRateId
			ORDER BY
			StartDate
			FOR XML AUTO, ELEMENTS
			) + ''</programIO>'';	

			Set @ErrorMessage = null;
		End
	Else
		Begin
			Set @ResultXml = null
			Set @ErrorMessage = ''Rate cannot be cancelled'';
		End
END

' 
END

GRANT  EXECUTE  ON [dbo].[CancelRate]  TO [CRSUser]

GO
