
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Fund_UpdateDateModified]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[Fund_UpdateDateModified] 
ON [dbo].[Fund] FOR UPDATE
AS 
SET NOCOUNT ON
UPDATE U SET U.DateModified = GETDATE()
FROM inserted i inner join Fund U 
on U.FundId = i.FundId





SET ANSI_NULLS ON
' 
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[RatePeriod_UpdateDateModified]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[RatePeriod_UpdateDateModified] 
ON [dbo].[RatePeriod] FOR UPDATE
AS 
SET NOCOUNT ON
UPDATE U SET U.DateModified = GETDATE()
FROM inserted i inner join RatePeriod U 
on U.RatePeriodId = i.RatePeriodId





SET ANSI_NULLS ON

' 
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Task_UpdateDateModified]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[Task_UpdateDateModified] 
ON [dbo].[Task] FOR UPDATE
AS 
SET NOCOUNT ON
UPDATE U SET U.DateModified = GETDATE()
FROM inserted i inner join Task U 
on U.TaskId = i.TaskId





SET ANSI_NULLS ON
' 
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[RateType_UpdateDateModified]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[RateType_UpdateDateModified] 
ON [dbo].[RateType] FOR UPDATE
AS 
SET NOCOUNT ON
UPDATE U SET U.DateModified = GETDATE()
FROM inserted i inner join RateType U 
on U.RateTypeId = i.RateTypeId





SET ANSI_NULLS ON
' 
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Rate_Audit]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[Rate_Audit] 
ON [dbo].[Rate] FOR UPDATE
AS 
SET NOCOUNT ON

declare @RateId bigint
declare @WorkFlowHistoryId bigint
declare @WorkFlowId bigint

	SELECT @RateId = RateId, @WorkFlowId =  WorkFlowId          
	FROM inserted

	SELECT @WorkFlowHistoryId = Max(WorkFlowHistoryId)
	FROM [dbo].[WorkFlowHistory]
	WHERE WorkFlowId = @WorkFlowId

	INSERT INTO [dbo].[RateAudit]
			   ([RateId]
			   ,[RateTypeId]
			   ,[StartDate]
			   ,[EndDate]
			   ,[SetDate]
			   ,[CreditingRate]
			   ,[ConvertedRate]
			   ,[RatePeriodId]
			   ,[MonthlyUseRate]
			   ,[R2Rate]
			   ,[SuperbRate]
			   ,[CalibreRate]
			   ,[InvestmentId]
			   ,[DateCreated]
			   ,[CreatedBy]
			   ,[DateModified]
			   ,[ModifiedBy]
               ,[WorkFlowHistoryId]
               ,[FundRuleId])
	SELECT [RateId]
		  ,[RateTypeId]
		  ,[StartDate]
		  ,[EndDate]
		  ,[SetDate]
		  ,[CreditingRate]
		  ,[ConvertedRate]
		  ,[RatePeriodId]
		  ,[MonthlyUseRate]
		  ,[R2Rate]
		  ,[SuperbRate]
		  ,[CalibreRate]		  
		  ,[InvestmentId]
		  ,[DateCreated]
		  ,[CreatedBy]
		  ,[DateModified]
		  ,[ModifiedBy]
          ,@WorkFlowHistoryId
          ,[FundRuleId]
	FROM deleted
	WHERE RateId = @RateId
' 
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[FundRule_UpdateDateModified]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[FundRule_UpdateDateModified] 
ON [dbo].[FundRule] FOR UPDATE
AS 
SET NOCOUNT ON
UPDATE U SET U.DateModified = GETDATE()
FROM inserted i inner join FundRule U 
on U.FundRuleId = i.FundRuleId





SET ANSI_NULLS ON
' 
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Investment_UpdateDateModified]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[Investment_UpdateDateModified] 
ON [dbo].[Investment] FOR UPDATE
AS 
SET NOCOUNT ON
UPDATE U SET U.DateModified = GETDATE()
FROM inserted i inner join Investment U 
on U.InvestmentId = i.InvestmentId





SET ANSI_NULLS ON
' 
GO




SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[WorkFlow_Audit]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[WorkFlow_Audit] 
ON [dbo].[WorkFlow] FOR UPDATE
AS 
BEGIN

	SET NOCOUNT ON
	declare @WorkFlowId bigint
	declare @WorkFlowStatusId bigint
	declare @AssignTo nvarchar(10)
	declare @DateTime datetime

	SELECT @WorkFlowId = [WorkFlowId], 
			@WorkFlowStatusId = [WorkFlowStatusId], 
			@AssignTo = [AssignTo],              
			@DateTime = [DateTime]
	FROM inserted

	IF (@WorkFlowId > 0)
	BEGIN
		INSERT INTO [dbo].[WorkFlowHistory]
			   ([WorkFlowId]
			   ,[WorkFlowStatusId]
			   ,[AssignTo]
			   ,[DateTime])
		VALUES
			   (
				@WorkFlowId
			   ,@WorkFlowStatusId
			   ,@AssignTo
			   ,@DateTime
			   )
	END
END
' 
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[WorkFlow_History]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[WorkFlow_History] 
ON [dbo].[WorkFlow] FOR UPDATE
AS 
SET NOCOUNT ON

declare @WorkFlowId bigint

BEGIN TRY
	BEGIN TRANSACTION

	SELECT @WorkFlowId = WorkFlowId 
	FROM inserted

	INSERT INTO [dbo].[WorkFlowHistory]
           (
             [WorkFlowId]
            ,[WorkFlowStatusId]
            ,[AssignTo]
            ,[DateTime]
            )    
	SELECT 
             [WorkFlowId]
	        ,[WorkFlowStatusId]
            ,[AssignTo]      
            ,[DateTime]  
	FROM deleted
	WHERE WorkFlowId = @WorkFlowId

	COMMIT TRANSACTION
END TRY
BEGIN CATCH
  ROLLBACK TRANSACTION
END CATCH
' 
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Schedule_UpdateDateModified]'))
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[Schedule_UpdateDateModified] 
ON [dbo].[Schedule] FOR UPDATE
AS 
SET NOCOUNT ON
UPDATE U SET U.DateModified = GETDATE()
FROM inserted i inner join Schedule U 
on U.ScheduleId = i.ScheduleId





SET ANSI_NULLS ON
' 
GO



