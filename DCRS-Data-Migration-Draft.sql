
--
-- DCRS Data Migration Script
--


-- Query #1 - Insert Funds
-- Run in DCRS MS Access database to generate the TSQL INSERT statements for the new CRS SQL 2005 database

select
'INSERT INTO Fund ' + 
'(FundName, R2FundId, TargetDateShiftDays, FYTDCorporateValidation, FYTDPensionValidation, ValidationTolerance, ' + 
'Disabled, CreatedBy, DateCreated) ' +
'SELECT ''' + Full_Name + ''', ' +
'0, ' +
'0, ' +
'0, ' +
'0, ' +
'0, ' +
'0, ' +
'''System'', getdate() ' + 
'FROM Fund WHERE FundName=''' + Full_Name + ''' HAVING count(FundId)=0' 
from
tblFundNames



-- Query #2 - Update Funds with R2FundId
-- Run in CRS SQL 2005 database

UPDATE Fund SET R2FundId=1 WHERE FundName='AGEST'
UPDATE Fund SET R2FundId=12 WHERE FundName='TISS'
--UPDATE Fund SET R2FundId=15 WHERE FundName='AUSTQ'
UPDATE Fund SET R2FundId=16 WHERE FundName='WEST'
UPDATE Fund SET R2FundId=18 WHERE FundName='STA'
UPDATE Fund SET R2FundId=19 WHERE FundName='CBUS'
--UPDATE Fund SET R2FundId=20 WHERE FundName='BERT'
UPDATE Fund SET R2FundId=21 WHERE FundName='FINSUPER'
--UPDATE Fund SET R2FundId=22 WHERE FundName='CIPQ'
UPDATE Fund SET R2FundId=23 WHERE FundName='HESTA'
--UPDATE Fund SET R2FundId=24 WHERE FundName='BERT2'
UPDATE Fund SET R2FundId=25 WHERE FundName='ARF'
--UPDATE Fund SET R2FundId=26 WHERE FundName='BEWT'
UPDATE Fund SET R2FundId=27 WHERE FundName='AUSSUPER'
--UPDATE Fund SET R2FundId=3 WHERE FundName='GIST'
UPDATE Fund SET R2FundId=4 WHERE FundName='HOST'
--UPDATE Fund SET R2FundId=5 WHERE FundName='APF'
UPDATE Fund SET R2FundId=6 WHERE FundName='HIP'
UPDATE Fund SET R2FundId=7 WHERE FundName='ISST'
UPDATE Fund SET R2FundId=8 WHERE FundName='MTAA'
UPDATE Fund SET R2FundId=9 WHERE FundName='JUST'
UPDATE Fund SET R2FundId=991 WHERE FundName='VIRGINSUPER'
UPDATE Fund SET R2FundId=992 WHERE FundName='CBUSSS'

/*
-- Generated from dev data

select 
'UPDATE Fund SET R2FundId=' + R2FundId + ' WHERE FundName=''' + FundName + ''''
from Fund
order by R2FundId

*/



-- Query #3 - Update Daily Rates Fund(s) with Daily Rates Web Service Settings
-- Run in CRS SQL 2005 database

UPDATE Fund SET DailyRatesEAIWS='http://spthd20:5565/ws/crsApp.webservices.provider:GetDailyRatesXml', TargetDateShiftDays=1, FYTDCorporateValidation=1, FYTDPensionValidation=1, ValidationTolerance=0.001 WHERE FundName='AUSSUPER'

/*
-- Generated from dev data

select 
'UPDATE Fund SET ' +
'DailyRatesEAIWS=''' + DailyRatesEAIWS + ''', ' +
'TargetDateShiftDays=' + cast(TargetDateShiftDays as nvarchar(max)) + ', ' +
'FYTDCorporateValidation=' + cast(FYTDCorporateValidation as nvarchar(max)) + ', ' +
'FYTDPensionValidation=' + cast(FYTDPensionValidation as nvarchar(max)) + ', ' +
'ValidationTolerance=' + cast(ValidationTolerance as nvarchar(max)) + ' ' +
'WHERE FundName=''' + FundName + ''''
from Fund
where DailyRatesEAIWS is not null and DailyRatesEAIWS <> ''

*/


-- Query #4 - Associate all funds to Global Admin user account
-- Run in CRS SQL 2005 database

-- purge all existing user funds associations
delete from UserFunds

-- create cursor to traverse all funds and create fund assocs for Global Admin user
declare @fundId bigint
declare AssociateAdminUserFunds cursor FAST_FORWARD
for
	select FundId from Fund order by FundId

open AssociateAdminUserFunds;

fetch Next from AssociateAdminUserFunds into 
	@fundId

WHILE (@@fetch_status <> -1)
BEGIN
	insert into UserFunds
		(Username, FundId, CreatedBy, DateCreated)
	values
		('crsadmin', @fundId, 'system', getdate())

	IF @@ERROR <> 0
	BEGIN
		close AssociateAdminUserFunds;
		deallocate AssociateAdminUserFunds;
		print 'Error occurred while inserting'
		return
	END

	-- Fetch next row

	fetch Next from AssociateAdminUserFunds into 
		@fundId
END

close AssociateAdminUserFunds;
deallocate AssociateAdminUserFunds;



-- Query #5 - General Setup of Lookup Data - Insert All Rate Periods (daily and non-daily), Rate Types, System Config, Work Flow Statuses
-- Run in CRS SQL 2005 database

SET IDENTITY_INSERT dbo.RatePeriod ON;
INSERT dbo.RatePeriod(RatePeriodId,RatePeriodCode,RatePeriodDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('1','pw','Per Week','System',convert(datetime,'2008-11-05 15:46:43.570',121),NULL,convert(datetime,NULL,121))
INSERT dbo.RatePeriod(RatePeriodId,RatePeriodCode,RatePeriodDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('2','pm','Per Month','System',convert(datetime,'2008-11-05 15:47:01.037',121),NULL,convert(datetime,NULL,121))
INSERT dbo.RatePeriod(RatePeriodId,RatePeriodCode,RatePeriodDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('3','pa','Per Annum','System',convert(datetime,'2008-11-05 15:49:32.000',121),'System',convert(datetime,'2009-09-01 12:22:16.470',121))
INSERT dbo.RatePeriod(RatePeriodId,RatePeriodCode,RatePeriodDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('4','pd','Per Day (Daily)','System',convert(datetime,'2009-05-25 15:35:29.000',121),'System',convert(datetime,'2009-05-26 10:11:18.643',121))
INSERT dbo.RatePeriod(RatePeriodId,RatePeriodCode,RatePeriodDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('5','ph','Per 6 Months','System',convert(datetime,'2009-08-27 14:12:07.000',121),NULL,convert(datetime,NULL,121))
SET IDENTITY_INSERT dbo.RatePeriod OFF;

SET IDENTITY_INSERT dbo.RateType ON;
INSERT dbo.RateType(RateTypeId,RateTypeCode,RateTypeInterfaceCode,RateTypeDescription,DateCreated,CreatedBy,DateModified,ModifiedBy) VALUES('1','DEC','FINAL','Final (Declared)',convert(datetime,'2008-11-05 15:37:52.000',121),'System',convert(datetime,'2009-09-01 12:21:07.933',121),'System')
INSERT dbo.RateType(RateTypeId,RateTypeCode,RateTypeInterfaceCode,RateTypeDescription,DateCreated,CreatedBy,DateModified,ModifiedBy) VALUES('2','INT','INTERIM','Interim (Intermediate)',convert(datetime,'2008-11-05 15:38:33.000',121),'System',convert(datetime,'2009-07-16 16:38:12.843',121),'System')
INSERT dbo.RateType(RateTypeId,RateTypeCode,RateTypeInterfaceCode,RateTypeDescription,DateCreated,CreatedBy,DateModified,ModifiedBy) VALUES('3','SPC','SPECIAL','Special',convert(datetime,'2009-02-12 14:24:57.443',121),'System',convert(datetime,'2009-07-16 16:38:17.703',121),NULL)
SET IDENTITY_INSERT dbo.RateType OFF;

SET IDENTITY_INSERT dbo.SystemConfig ON;
INSERT dbo.SystemConfig(ConfigId,ConfigName,ConfigValue,Updateable) VALUES('1','LastSuccessfulReconcile','28/05/2009','0')
INSERT dbo.SystemConfig(ConfigId,ConfigName,ConfigValue,Updateable) VALUES('2','ReconcileR2','yes','1')
INSERT dbo.SystemConfig(ConfigId,ConfigName,ConfigValue,Updateable) VALUES('3','ReconcileSuperb','yes','1')
INSERT dbo.SystemConfig(ConfigId,ConfigName,ConfigValue,Updateable) VALUES('4','ReconcileCalibre','no','1')
INSERT dbo.SystemConfig(ConfigId,ConfigName,ConfigValue,Updateable) VALUES('5','NotificationEmailAddress','anilkanth@superpartners.com.au','1')
SET IDENTITY_INSERT dbo.SystemConfig OFF;

SET IDENTITY_INSERT dbo.WorkFlowStatus ON;
INSERT dbo.WorkFlowStatus(WorkFlowStatusId,WorkFlowSequenceId,WorkFlowStatusCode,WorkFlowStatusDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('1','1','Created','Created','system',convert(datetime,'2009-08-03 18:03:38.173',121),NULL,convert(datetime,NULL,121))
INSERT dbo.WorkFlowStatus(WorkFlowStatusId,WorkFlowSequenceId,WorkFlowStatusCode,WorkFlowStatusDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('2','2','Modified','Modified','system',convert(datetime,'2009-08-03 18:03:38.173',121),NULL,convert(datetime,NULL,121))
INSERT dbo.WorkFlowStatus(WorkFlowStatusId,WorkFlowSequenceId,WorkFlowStatusCode,WorkFlowStatusDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('3','3','Deleted','Deleted','system',convert(datetime,'2009-08-03 18:03:38.173',121),NULL,convert(datetime,NULL,121))
INSERT dbo.WorkFlowStatus(WorkFlowStatusId,WorkFlowSequenceId,WorkFlowStatusCode,WorkFlowStatusDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('4','4','VerificationReminderSent','Verification Reminder Sent','system',convert(datetime,'2009-08-03 18:03:38.173',121),NULL,convert(datetime,NULL,121))
INSERT dbo.WorkFlowStatus(WorkFlowStatusId,WorkFlowSequenceId,WorkFlowStatusCode,WorkFlowStatusDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('5','5','Verified','Verified','system',convert(datetime,'2009-08-03 18:03:38.173',121),NULL,convert(datetime,NULL,121))
INSERT dbo.WorkFlowStatus(WorkFlowStatusId,WorkFlowSequenceId,WorkFlowStatusCode,WorkFlowStatusDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('6','6','ApprovalReminderSent','Approval Reminder Sent','system',convert(datetime,'2009-08-03 18:03:38.190',121),NULL,convert(datetime,NULL,121))
INSERT dbo.WorkFlowStatus(WorkFlowStatusId,WorkFlowSequenceId,WorkFlowStatusCode,WorkFlowStatusDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('7','7','Approved','Approved','system',convert(datetime,'2009-08-03 18:03:38.190',121),NULL,convert(datetime,NULL,121))
INSERT dbo.WorkFlowStatus(WorkFlowStatusId,WorkFlowSequenceId,WorkFlowStatusCode,WorkFlowStatusDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('8','8','Exported','Exported','system',convert(datetime,'2009-08-03 18:03:38.190',121),NULL,convert(datetime,NULL,121))
INSERT dbo.WorkFlowStatus(WorkFlowStatusId,WorkFlowSequenceId,WorkFlowStatusCode,WorkFlowStatusDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('9','9','ExportFailed','Export Failed','system',convert(datetime,'2009-08-03 18:03:38.190',121),NULL,convert(datetime,NULL,121))
INSERT dbo.WorkFlowStatus(WorkFlowStatusId,WorkFlowSequenceId,WorkFlowStatusCode,WorkFlowStatusDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('10','10','Confirmed','Confirmed','system',convert(datetime,'2009-08-03 18:03:38.190',121),NULL,convert(datetime,NULL,121))
SET IDENTITY_INSERT dbo.WorkFlowStatus OFF;



-- Query #6 - Insert Daily Rates Investments
-- Run in DCRS MS Access database to generate the TSQL INSERT statements for the new CRS SQL 2005 database
-- Note: Two Investment OPtions (BHPOPTC and BHPOPTD) are disabled (marked to fund AUSSUPEREX), and hence will not be transferred

select
'INSERT INTO Investment ' + 
'(FundId, InvestmentCode, InvestmentDescription, InterimRateCode, DeclaredRateCode, ' +
'GPenCode, GPenFundCode, IsDefault, PWRRule, SwitchRule, XmlFlag, InvestmentType, SuperbDeclaredCode, ' +
'CalibreDeclaredCode, SuperbInterimCode, CalibreInterimCode, DateCreated, CreatedBy) ' +
'SELECT FundId, ' +
'''' + Investment_Code + ''', ' +
'''' + Investment_Description + ''', ' +
'''' + Interim_Rate_Code + ''', ' +
'''' + Declared_Rate_Code + ''', ' +
iif(isnull(GpenCode), '0', cstr(GpenCode)) + ', ' +
'''' + iif(isnull(GpenFundID), '', GpenFundID) + ''', ' +
cstr([Default]) + ', ' +
'''' + iif(isnull(PWPRule), '', PWPRule) + ''', ' +
'''' + iif(isnull(SWITCHRule), '', SWITCHRule) + ''', ' +
cstr([XML_Flag]) + ', ' +
'''' + Investment_Type + ''', ' +
'''' + iif(isnull(SuperbDeclaredCode), '', SuperbDeclaredCode) + ''', ' +
'''' + iif(isnull(CalibreDeclaredCode), '', CalibreDeclaredCode) + ''', ' +
'''' + iif(isnull(SuperbInterimCode), '', SuperbInterimCode) + ''', ' +
'''' + iif(isnull(CalibreInterimCode), '', CalibreInterimCode) + ''', ' +
'getdate(), ''System'' ' + 
'FROM Fund WHERE FundName=''' + [Fund] + '''' 
from
tblInvestmentCodes



-- Query #7 - Insert Daily Rates Fund Rules
-- Run in DCRS MS Access database to generate the TSQL INSERT statements for the new CRS SQL 2005 database
-- Note: Two Fund Rules for Investment Options (BHPOPTC and BHPOPTD) will not be transferred

select
'INSERT INTO FundRule ' + 
'(InvestmentId, RateTypeId, RatePeriodId, LastDate, RuleCode, DefaultRule, ' + 
'DefaultRuleCode, R2RuleCode, SuperbRuleCode, CalibreRuleCode, R2Dec, SuperbDec, ' + 
'CalibreDec, R2RuleType, SuperbRuleType, CalibreRuleType, CreatedBy, DateCreated) ' +
'SELECT InvestmentId, ' +
'1, ' +
'4, ' +
'''' + format(dLastDate, 'yyyy-mm-dd') + ''', ' +
'''' + RuleCode + ''', ' +
'''' + Default + ''', ' +
'''' + DefaultRuleCode + ''', ' +
'''' + iif(isnull(R2RuleCode), '', R2RuleCode) + ''' , ' +
'''' + iif(isnull(SuperbRuleCode), '', SuperbRuleCode) + ''', ' +
'''' + iif(isnull(CalibreRuleCode), '', CalibreRuleCode) + ''', ' +
'''' + iif(isnull(R2Dec), '', R2Dec) + ''', ' +
'''' + iif(isnull(SuperbDec), '', SuperbDec) + ''', ' +
'''' + iif(isnull(CalibreDec), '', cstr(CalibreDec)) + ''', ' +
'''' + iif(isnull(R2RuleType), '', R2RuleType) + ''', ' +
'''' + iif(isnull(SuperbRuleType), '', SuperbRuleType) + ''', ' +
'''' + iif(isnull(CalibreRuleType), '', CalibreRuleType) + ''', ' + 
'''System'', getdate() ' + 
'FROM Investment WHERE InvestmentCode=''' + Investment_Code + '''' 
from
tblFundRules



-- Query #8 - Insert Dummy Workflow and assign to dummy fund
-- Run in CRS SQL 2005 database

INSERT INTO [dbo].[WorkFlow]
           ([AssignTo]
           ,[WorkFlowStatusId]
           ,[FundId]
           ,[DateTime]
           ,[DateExported]
           ,[ReconciledR2]
           ,[ReconciledSuperb]
           ,[ReconciledCalibre])
     SELECT 'systemp',
				(select WorkFlowStatusId from dbo.WorkFlowStatus where WorkFlowStatusCode='Exported'),
				FundId,getdate(),getdate(),1,1,0 
				FROM dbo.Fund 
				WHERE FundName='AUSSUPER'



-- Query #9 - Insert Daily Rates
-- Run in DCRS MS Access database to generate the TSQL INSERT statements for the new CRS SQL 2005 database
-- Note: Rates for Investment Options (BHPOPTC and BHPOPTD) will not be transferred

select
'INSERT INTO Rate ' + 
'(FundRuleId, InvestmentId, WorkFlowId, RatePeriodId, RateTypeId, StartDate, ' + 
'EndDate, SetDate, CreditingRate, ConvertedRate, PensionRate, MonthlyUseRate, R2Rate, ' +
'SuperbRate, CalibreRate, CreatedBy, DateCreated, ModifiedBy, DateModified, Cancelled) ' +
'SELECT fr.FundRuleId, fr.InvestmentId, (select top 1 WorkFlowId from dbo.WorkFlow order by WorkFlowId), fr.RatePeriodId, fr.RateTypeId, ' +
'''' + format(Start_Date, 'yyyy-mm-dd') + ''', ' +
'''' + format(End_Date, 'yyyy-mm-dd') + ''', ' +
'''' + format(Set_Date, 'yyyy-mm-dd') + ''', ' +
iif(isnull(Corporate_Rate), 'null', cstr(Corporate_Rate)) + ' ,' +
iif(isnull(Corporate_Rate), 'null', cstr(Corporate_Rate)) + ' ,' +
iif(isnull(Pension_Rate), 'null', cstr(Pension_Rate)) + ' ,' +
'null, ' +
iif(isnull(R2_Rate), 'null', '''' + cstr(R2_Rate) + '''') + ' ,' +
iif(isnull(Superb_Rate), 'null', '''' + cstr(Superb_Rate) + '''') + ' ,' +
iif(isnull(Calibre_Rate), 'null', '''' + cstr(Calibre_Rate) + '''') + ' ,' +
'''' + User + ''', ''' + format(DateStamp, 'yyyy-mm-dd') + ''', ' + 
'''System'', getdate(), ' + 
'0 ' +
'FROM FundRule fr, Investment i WHERE fr.InvestmentId=i.InvestmentId AND ' +
'i.InvestmentCode=''' + Rate_Investment_Code + ''' AND fr.RateTypeId=1 AND fr.RatePeriodId=4 ' 
from
tblRates
where Set_Date between #2008-07-01# and #2009-10-29#
order by Set_Date




-- Query #10 - Create multiple Workflows for all daily rates
-- Run in CRS SQL 2005 database


BEGIN TRY
	BEGIN TRANSACTION

		declare @SetDate datetime
		declare @CreatedBy nvarchar(10)
		declare @DateCreated datetime

		declare @AusSuperFundId bigint
		declare @ConfirmedWorkFlowStatusId bigint
		declare @NewWorkFlowId bigint

		select @AusSuperFundId=FundId
		from Fund where FundName='AUSSUPER'

		select @ConfirmedWorkFlowStatusId=WorkFlowStatusId
		from WorkFlowStatus where WorkFlowStatusCode='Confirmed'

		Declare AllSetDates Cursor FAST_FORWARD
		For 
			select SetDate, CreatedBy, DateCreated from Rate
			group by SetDate, CreatedBy, DateCreated
			order by SetDate

		Open AllSetDates;

		Fetch Next from AllSetDates Into 
			@SetDate, @CreatedBy, @DateCreated

		WHILE (@@fetch_status <> -1)
		BEGIN

			INSERT INTO [dbo].[WorkFlow]
					   ([AssignTo]
					   ,[WorkFlowStatusId]
					   ,[FundId]
					   ,[DateTime]
					   ,[DateExported]
					   ,[ReconciledR2]
					   ,[ReconciledSuperb]
					   ,[ReconciledCalibre])
				 VALUES
					   (@CreatedBy,
						@ConfirmedWorkFlowStatusId,
						@AusSuperFundId,
						@DateCreated,
						@DateCreated,
						1,
						1,
						0)

			select @NewWorkFlowId = SCOPE_IDENTITY()

			update Rate set WorkFlowId=@NewWorkFlowId where SetDate=@SetDate

			-- Fetch next row

			Fetch Next from AllSetDates Into 
				@SetDate, @CreatedBy, @DateCreated
		END;

		-- Cleanup Cursor

		Close AllSetDates;
		DeAllocate AllSetDates;

		delete from WorkFlow where AssignTo='systemp'

	COMMIT TRANSACTION
END TRY
BEGIN CATCH

	Close AllSetDates;
	DeAllocate AllSetDates;
	
	print 'Error occurred: ' + cast(ERROR_NUMBER() as nvarchar(50)) + ' - ' + ERROR_MESSAGE();

	ROLLBACK TRANSACTION
END CATCH

