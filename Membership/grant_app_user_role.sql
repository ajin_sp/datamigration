DECLARE @SQLSTMT NVARCHAR(1000)

DECLARE C_SQL1 CURSOR FOR
select 'grant execute on ' + '['+ schema_name(uid) + '].['+ name + '] to APPLICATION_USER_ROLE'
from sysobjects
where xtype in ('P','FN')
UNION
select 'grant execute, references on TYPE::['+schema_name(schema_id)+'].[' + name + '] to APPLICATION_USER_ROLE'
from sys.types
where is_user_defined = 1
ORDER BY 1


OPEN C_SQL1
while 1=1
begin
        fetch C_SQL1 into @SQLSTMT
        if @@FETCH_STATUS != 0 break
        PRINT @SQLSTMT
        EXEC SP_EXECUTESQL @SQLSTMT

end
CLOSE C_SQL1
deallocate C_SQL1
