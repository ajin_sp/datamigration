SET NOCOUNT ON
-- Restricts volume of output to errors and
-- messages that use the PRINT function


PRINT 'INSERTING DATA INTO TABLE aspnet_Applications'
ALTER TABLE [aspnet_Applications] NOCHECK CONSTRAINT ALL
INSERT INTO [aspnet_Applications] ([ApplicationName],[LoweredApplicationName],[ApplicationId],[Description]) VALUES ('ProjectCRS','projectcrs','74f73fb5-1325-4e1c-83c0-3f1508cffbe0',NULL)
ALTER TABLE [aspnet_Applications] CHECK CONSTRAINT ALL

PRINT 'INSERTING DATA INTO TABLE aspnet_Users'
ALTER TABLE [aspnet_Users] NOCHECK CONSTRAINT ALL
INSERT INTO [aspnet_Users] ([ApplicationId],[UserId],[UserName],[LoweredUserName],[MobileAlias],[IsAnonymous],[LastActivityDate]) VALUES ('74f73fb5-1325-4e1c-83c0-3f1508cffbe0','7a2de081-22b5-4a59-b004-b67151c0aa34','crsadmin','crsadmin',NULL,0,'2010/03/03 11:24:16 PM')
ALTER TABLE [aspnet_Users] CHECK CONSTRAINT ALL

PRINT 'INSERTING DATA INTO TABLE aspnet_Membership'
ALTER TABLE [aspnet_Membership] NOCHECK CONSTRAINT ALL
INSERT INTO [aspnet_Membership] ([ApplicationId],[UserId],[Password],[PasswordFormat],[PasswordSalt],[MobilePIN],[Email],[LoweredEmail],[PasswordQuestion],[PasswordAnswer],[IsApproved],[IsLockedOut],[CreateDate],[LastLoginDate],[LastPasswordChangedDate],[LastLockoutDate],[FailedPasswordAttemptCount],[FailedPasswordAttemptWindowStart],[FailedPasswordAnswerAttemptCount],[FailedPasswordAnswerAttemptWindowStart],[Comment]) VALUES ('74f73fb5-1325-4e1c-83c0-3f1508cffbe0','7a2de081-22b5-4a59-b004-b67151c0aa34','s9vk4MUoBzQGZ3pfv6itKAG6jCI=',1,'ECZkWzFeA8Y6xD/GaZClAg==',NULL,'testing@superpartners.com.au','testing@superpartners.com.au',NULL,NULL,1,0,'2010/03/03 11:07:44 PM','2010/03/03 11:24:16 PM','2010/03/03 11:07:44 PM','1754/01/01 12:00:00 AM',0,'1754/01/01 12:00:00 AM',0,'1754/01/01 12:00:00 AM',NULL)
ALTER TABLE [aspnet_Membership] CHECK CONSTRAINT ALL

PRINT 'INSERTING DATA INTO TABLE aspnet_Roles'
ALTER TABLE [aspnet_Roles] NOCHECK CONSTRAINT ALL
INSERT INTO [aspnet_Roles] ([ApplicationId],[RoleId],[RoleName],[LoweredRoleName],[Description]) VALUES ('74f73fb5-1325-4e1c-83c0-3f1508cffbe0','31bdf902-dce5-42c5-9003-04fe29e0b7c4','Administrator','administrator',NULL)
INSERT INTO [aspnet_Roles] ([ApplicationId],[RoleId],[RoleName],[LoweredRoleName],[Description]) VALUES ('74f73fb5-1325-4e1c-83c0-3f1508cffbe0','f22218c2-c2ea-4fe8-bc2a-eab96e3d3a4c','Approver','approver',NULL)
INSERT INTO [aspnet_Roles] ([ApplicationId],[RoleId],[RoleName],[LoweredRoleName],[Description]) VALUES ('74f73fb5-1325-4e1c-83c0-3f1508cffbe0','10e75104-f41d-4d0f-8a6c-866693d3ee00','Global Admin','global admin',NULL)
INSERT INTO [aspnet_Roles] ([ApplicationId],[RoleId],[RoleName],[LoweredRoleName],[Description]) VALUES ('74f73fb5-1325-4e1c-83c0-3f1508cffbe0','52584ad9-32bf-4124-bd82-4df84baee9ad','Reviewer','reviewer',NULL)
INSERT INTO [aspnet_Roles] ([ApplicationId],[RoleId],[RoleName],[LoweredRoleName],[Description]) VALUES ('74f73fb5-1325-4e1c-83c0-3f1508cffbe0','ddc46a46-8b6a-4108-b238-22b1d53c0b81','User','user',NULL)
ALTER TABLE [aspnet_Roles] CHECK CONSTRAINT ALL

PRINT 'INSERTING DATA INTO TABLE aspnet_SchemaVersions'
ALTER TABLE [aspnet_SchemaVersions] NOCHECK CONSTRAINT ALL
INSERT INTO [aspnet_SchemaVersions] ([Feature],[CompatibleSchemaVersion],[IsCurrentVersion]) VALUES ('common','1',1)
INSERT INTO [aspnet_SchemaVersions] ([Feature],[CompatibleSchemaVersion],[IsCurrentVersion]) VALUES ('health monitoring','1',1)
INSERT INTO [aspnet_SchemaVersions] ([Feature],[CompatibleSchemaVersion],[IsCurrentVersion]) VALUES ('membership','1',1)
INSERT INTO [aspnet_SchemaVersions] ([Feature],[CompatibleSchemaVersion],[IsCurrentVersion]) VALUES ('personalization','1',1)
INSERT INTO [aspnet_SchemaVersions] ([Feature],[CompatibleSchemaVersion],[IsCurrentVersion]) VALUES ('profile','1',1)
INSERT INTO [aspnet_SchemaVersions] ([Feature],[CompatibleSchemaVersion],[IsCurrentVersion]) VALUES ('role manager','1',1)
ALTER TABLE [aspnet_SchemaVersions] CHECK CONSTRAINT ALL

PRINT 'INSERTING DATA INTO TABLE aspnet_UsersInRoles'
ALTER TABLE [aspnet_UsersInRoles] NOCHECK CONSTRAINT ALL
INSERT INTO [aspnet_UsersInRoles] ([UserId],[RoleId]) VALUES ('7a2de081-22b5-4a59-b004-b67151c0aa34','10e75104-f41d-4d0f-8a6c-866693d3ee00')
ALTER TABLE [aspnet_UsersInRoles] CHECK CONSTRAINT ALL
