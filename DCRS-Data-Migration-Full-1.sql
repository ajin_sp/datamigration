
--
-- DCRS Data Migration Script - Part 1 (Auto Execution)
--


-- Set #1 - Insert Funds

INSERT INTO Fund (FundName, R2FundId, TargetDateShiftDays, FYTDCorporateValidation, FYTDPensionValidation, ValidationTolerance, Disabled, CreatedBy, DateCreated) SELECT 'AGEST', 0, 0, 0, 0, 0, 0, 'System', getdate() FROM Fund WHERE FundName='AGEST' HAVING count(FundId)=0
INSERT INTO Fund (FundName, R2FundId, TargetDateShiftDays, FYTDCorporateValidation, FYTDPensionValidation, ValidationTolerance, Disabled, CreatedBy, DateCreated) SELECT 'CBUS', 0, 0, 0, 0, 0, 0, 'System', getdate() FROM Fund WHERE FundName='CBUS' HAVING count(FundId)=0
INSERT INTO Fund (FundName, R2FundId, TargetDateShiftDays, FYTDCorporateValidation, FYTDPensionValidation, ValidationTolerance, Disabled, CreatedBy, DateCreated) SELECT 'FINSUPER', 0, 0, 0, 0, 0, 0, 'System', getdate() FROM Fund WHERE FundName='FINSUPER' HAVING count(FundId)=0
INSERT INTO Fund (FundName, R2FundId, TargetDateShiftDays, FYTDCorporateValidation, FYTDPensionValidation, ValidationTolerance, Disabled, CreatedBy, DateCreated) SELECT 'HESTA', 0, 0, 0, 0, 0, 0, 'System', getdate() FROM Fund WHERE FundName='HESTA' HAVING count(FundId)=0
INSERT INTO Fund (FundName, R2FundId, TargetDateShiftDays, FYTDCorporateValidation, FYTDPensionValidation, ValidationTolerance, Disabled, CreatedBy, DateCreated) SELECT 'HOST', 0, 0, 0, 0, 0, 0, 'System', getdate() FROM Fund WHERE FundName='HOST' HAVING count(FundId)=0
INSERT INTO Fund (FundName, R2FundId, TargetDateShiftDays, FYTDCorporateValidation, FYTDPensionValidation, ValidationTolerance, Disabled, CreatedBy, DateCreated) SELECT 'ISST', 0, 0, 0, 0, 0, 0, 'System', getdate() FROM Fund WHERE FundName='ISST' HAVING count(FundId)=0
INSERT INTO Fund (FundName, R2FundId, TargetDateShiftDays, FYTDCorporateValidation, FYTDPensionValidation, ValidationTolerance, Disabled, CreatedBy, DateCreated) SELECT 'JUST', 0, 0, 0, 0, 0, 0, 'System', getdate() FROM Fund WHERE FundName='JUST' HAVING count(FundId)=0
INSERT INTO Fund (FundName, R2FundId, TargetDateShiftDays, FYTDCorporateValidation, FYTDPensionValidation, ValidationTolerance, Disabled, CreatedBy, DateCreated) SELECT 'MTAA', 0, 0, 0, 0, 0, 0, 'System', getdate() FROM Fund WHERE FundName='MTAA' HAVING count(FundId)=0
INSERT INTO Fund (FundName, R2FundId, TargetDateShiftDays, FYTDCorporateValidation, FYTDPensionValidation, ValidationTolerance, Disabled, CreatedBy, DateCreated) SELECT 'HIP', 0, 0, 0, 0, 0, 0, 'System', getdate() FROM Fund WHERE FundName='HIP' HAVING count(FundId)=0
INSERT INTO Fund (FundName, R2FundId, TargetDateShiftDays, FYTDCorporateValidation, FYTDPensionValidation, ValidationTolerance, Disabled, CreatedBy, DateCreated) SELECT 'STA', 0, 0, 0, 0, 0, 0, 'System', getdate() FROM Fund WHERE FundName='STA' HAVING count(FundId)=0
INSERT INTO Fund (FundName, R2FundId, TargetDateShiftDays, FYTDCorporateValidation, FYTDPensionValidation, ValidationTolerance, Disabled, CreatedBy, DateCreated) SELECT 'TISS', 0, 0, 0, 0, 0, 0, 'System', getdate() FROM Fund WHERE FundName='TISS' HAVING count(FundId)=0
INSERT INTO Fund (FundName, R2FundId, TargetDateShiftDays, FYTDCorporateValidation, FYTDPensionValidation, ValidationTolerance, Disabled, CreatedBy, DateCreated) SELECT 'WEST', 0, 0, 0, 0, 0, 0, 'System', getdate() FROM Fund WHERE FundName='WEST' HAVING count(FundId)=0
INSERT INTO Fund (FundName, R2FundId, TargetDateShiftDays, FYTDCorporateValidation, FYTDPensionValidation, ValidationTolerance, Disabled, CreatedBy, DateCreated) SELECT 'ARF', 0, 0, 0, 0, 0, 0, 'System', getdate() FROM Fund WHERE FundName='ARF' HAVING count(FundId)=0
INSERT INTO Fund (FundName, R2FundId, TargetDateShiftDays, FYTDCorporateValidation, FYTDPensionValidation, ValidationTolerance, Disabled, CreatedBy, DateCreated) SELECT 'AUSSUPER', 0, 0, 0, 0, 0, 0, 'System', getdate() FROM Fund WHERE FundName='AUSSUPER' HAVING count(FundId)=0
INSERT INTO Fund (FundName, R2FundId, TargetDateShiftDays, FYTDCorporateValidation, FYTDPensionValidation, ValidationTolerance, Disabled, CreatedBy, DateCreated) SELECT 'VIRGINSUPER', 0, 0, 0, 0, 0, 0, 'System', getdate() FROM Fund WHERE FundName='VIRGINSUPER' HAVING count(FundId)=0
INSERT INTO Fund (FundName, R2FundId, TargetDateShiftDays, FYTDCorporateValidation, FYTDPensionValidation, ValidationTolerance, Disabled, CreatedBy, DateCreated) SELECT 'CBUSSS', 0, 0, 0, 0, 0, 0, 'System', getdate() FROM Fund WHERE FundName='CBUSSS' HAVING count(FundId)=0

GO

/*
-- Above set is generated from source data query

-- Run in DCRS MS Access database to generate the TSQL INSERT statements for the new CRS SQL 2005 database

select
'INSERT INTO Fund ' + 
'(FundName, R2FundId, TargetDateShiftDays, FYTDCorporateValidation, FYTDPensionValidation, ValidationTolerance, ' + 
'Disabled, CreatedBy, DateCreated) ' +
'SELECT ''' + Full_Name + ''', ' +
'0, ' +
'0, ' +
'0, ' +
'0, ' +
'0, ' +
'0, ' +
'''System'', getdate() ' + 
'FROM Fund WHERE FundName=''' + Full_Name + ''' HAVING count(FundId)=0' 
from
tblFundNames

*/


-- Set #2 - Update Funds with R2FundId

UPDATE Fund SET R2FundId=1 WHERE FundName='AGEST'
UPDATE Fund SET R2FundId=12 WHERE FundName='TISS'
--UPDATE Fund SET R2FundId=15 WHERE FundName='AUSTQ'
UPDATE Fund SET R2FundId=16 WHERE FundName='WEST'
UPDATE Fund SET R2FundId=18 WHERE FundName='STA'
UPDATE Fund SET R2FundId=19 WHERE FundName='CBUS'
--UPDATE Fund SET R2FundId=20 WHERE FundName='BERT'
UPDATE Fund SET R2FundId=21 WHERE FundName='FINSUPER'
--UPDATE Fund SET R2FundId=22 WHERE FundName='CIPQ'
UPDATE Fund SET R2FundId=23 WHERE FundName='HESTA'
--UPDATE Fund SET R2FundId=24 WHERE FundName='BERT2'
UPDATE Fund SET R2FundId=25 WHERE FundName='ARF'
--UPDATE Fund SET R2FundId=26 WHERE FundName='BEWT'
UPDATE Fund SET R2FundId=27 WHERE FundName='AUSSUPER'
--UPDATE Fund SET R2FundId=3 WHERE FundName='GIST'
UPDATE Fund SET R2FundId=4 WHERE FundName='HOST'
--UPDATE Fund SET R2FundId=5 WHERE FundName='APF'
UPDATE Fund SET R2FundId=6 WHERE FundName='HIP'
UPDATE Fund SET R2FundId=7 WHERE FundName='ISST'
UPDATE Fund SET R2FundId=8 WHERE FundName='MTAA'
UPDATE Fund SET R2FundId=9 WHERE FundName='JUST'
UPDATE Fund SET R2FundId=991 WHERE FundName='VIRGINSUPER'
UPDATE Fund SET R2FundId=992 WHERE FundName='CBUSSS'

GO

/*
-- Above set is generated from dev data query

select 
'UPDATE Fund SET R2FundId=' + R2FundId + ' WHERE FundName=''' + FundName + ''''
from Fund
order by R2FundId

*/



-- Set #3 - Update Daily Rates Fund(s) with Daily Rates Web Service Settings

UPDATE Fund SET DailyRatesEAIWS='http://spthd20:5565/ws/crsApp.webservices.provider:GetDailyRatesXml', TargetDateShiftDays=1, FYTDCorporateValidation=1, FYTDPensionValidation=1, ValidationTolerance=0.001 WHERE FundName='AUSSUPER'

GO

/*
-- Above set is generated from dev data query

select 
'UPDATE Fund SET ' +
'DailyRatesEAIWS=''' + DailyRatesEAIWS + ''', ' +
'TargetDateShiftDays=' + cast(TargetDateShiftDays as nvarchar(max)) + ', ' +
'FYTDCorporateValidation=' + cast(FYTDCorporateValidation as nvarchar(max)) + ', ' +
'FYTDPensionValidation=' + cast(FYTDPensionValidation as nvarchar(max)) + ', ' +
'ValidationTolerance=' + cast(ValidationTolerance as nvarchar(max)) + ' ' +
'WHERE FundName=''' + FundName + ''''
from Fund
where DailyRatesEAIWS is not null and DailyRatesEAIWS <> ''

*/


-- Set #4 - Create account settings for Super Admin user (crsadmin) and associate all funds to Super Admin user account

delete from UserSettings
GO
INSERT INTO UserSettings
           ([Username],[TermsCondDateAccepted],[InactivityPeriod])
     VALUES
           ('crsadmin',null,30)
GO

-- purge all existing user funds associations
delete from UserFunds

-- create cursor to traverse all funds and create fund assocs for Super Admin user
declare @fundId bigint
declare AssociateAdminUserFunds cursor FAST_FORWARD
for
	select FundId from Fund order by FundId

open AssociateAdminUserFunds;

fetch Next from AssociateAdminUserFunds into 
	@fundId

WHILE (@@fetch_status <> -1)
BEGIN
	insert into UserFunds
		(Username, FundId, CreatedBy, DateCreated)
	values
		('crsadmin', @fundId, 'system', getdate())

	IF @@ERROR <> 0
	BEGIN
		close AssociateAdminUserFunds;
		deallocate AssociateAdminUserFunds;
		print 'Error occurred while inserting'
		return
	END

	-- Fetch next row

	fetch Next from AssociateAdminUserFunds into 
		@fundId
END

close AssociateAdminUserFunds;
deallocate AssociateAdminUserFunds;

GO


-- Set #5 - General Setup of Lookup Data - Insert All Rate Periods (daily and non-daily), Rate Types, System Config, Work Flow Statuses

SET IDENTITY_INSERT dbo.RatePeriod ON;
INSERT dbo.RatePeriod(RatePeriodId,RatePeriodCode,RatePeriodDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('1','pw','Per Week','System',convert(datetime,'2008-11-05 15:46:43.570',121),NULL,convert(datetime,NULL,121))
INSERT dbo.RatePeriod(RatePeriodId,RatePeriodCode,RatePeriodDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('2','pm','Per Month','System',convert(datetime,'2008-11-05 15:47:01.037',121),NULL,convert(datetime,NULL,121))
INSERT dbo.RatePeriod(RatePeriodId,RatePeriodCode,RatePeriodDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('3','pa','Per Annum','System',convert(datetime,'2008-11-05 15:49:32.000',121),'System',convert(datetime,'2009-09-01 12:22:16.470',121))
INSERT dbo.RatePeriod(RatePeriodId,RatePeriodCode,RatePeriodDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('4','pd','Per Day (Daily)','System',convert(datetime,'2009-05-25 15:35:29.000',121),'System',convert(datetime,'2009-05-26 10:11:18.643',121))
INSERT dbo.RatePeriod(RatePeriodId,RatePeriodCode,RatePeriodDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('5','ph','Per 6 Months','System',convert(datetime,'2009-08-27 14:12:07.000',121),NULL,convert(datetime,NULL,121))
SET IDENTITY_INSERT dbo.RatePeriod OFF;

GO

SET IDENTITY_INSERT dbo.RateType ON;
INSERT dbo.RateType(RateTypeId,RateTypeCode,RateTypeInterfaceCode,RateTypeDescription,DateCreated,CreatedBy,DateModified,ModifiedBy) VALUES('1','DEC','FINAL','Final (Declared)',convert(datetime,'2008-11-05 15:37:52.000',121),'System',convert(datetime,'2009-09-01 12:21:07.933',121),'System')
INSERT dbo.RateType(RateTypeId,RateTypeCode,RateTypeInterfaceCode,RateTypeDescription,DateCreated,CreatedBy,DateModified,ModifiedBy) VALUES('2','INT','INTERIM','Interim (Intermediate)',convert(datetime,'2008-11-05 15:38:33.000',121),'System',convert(datetime,'2009-07-16 16:38:12.843',121),'System')
INSERT dbo.RateType(RateTypeId,RateTypeCode,RateTypeInterfaceCode,RateTypeDescription,DateCreated,CreatedBy,DateModified,ModifiedBy) VALUES('3','SPC','SPECIAL','Special',convert(datetime,'2009-02-12 14:24:57.443',121),'System',convert(datetime,'2009-07-16 16:38:17.703',121),NULL)
SET IDENTITY_INSERT dbo.RateType OFF;

GO

SET IDENTITY_INSERT dbo.SystemConfig ON;
INSERT dbo.SystemConfig(ConfigId,ConfigName,ConfigValue,Updateable) VALUES('1','LastSuccessfulReconcile','28/05/2009','0')
INSERT dbo.SystemConfig(ConfigId,ConfigName,ConfigValue,Updateable) VALUES('2','ReconcileR2','yes','1')
INSERT dbo.SystemConfig(ConfigId,ConfigName,ConfigValue,Updateable) VALUES('3','ReconcileSuperb','yes','1')
INSERT dbo.SystemConfig(ConfigId,ConfigName,ConfigValue,Updateable) VALUES('4','ReconcileCalibre','no','1')
INSERT dbo.SystemConfig(ConfigId,ConfigName,ConfigValue,Updateable) VALUES('5','NotificationEmailAddress','anilkanth@superpartners.com.au','1')
SET IDENTITY_INSERT dbo.SystemConfig OFF;

GO

SET IDENTITY_INSERT dbo.WorkFlowStatus ON;
INSERT dbo.WorkFlowStatus(WorkFlowStatusId,WorkFlowSequenceId,WorkFlowStatusCode,WorkFlowStatusDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('1','1','Created','Created','system',convert(datetime,'2009-08-03 18:03:38.173',121),NULL,convert(datetime,NULL,121))
INSERT dbo.WorkFlowStatus(WorkFlowStatusId,WorkFlowSequenceId,WorkFlowStatusCode,WorkFlowStatusDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('2','2','Modified','Modified','system',convert(datetime,'2009-08-03 18:03:38.173',121),NULL,convert(datetime,NULL,121))
INSERT dbo.WorkFlowStatus(WorkFlowStatusId,WorkFlowSequenceId,WorkFlowStatusCode,WorkFlowStatusDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('3','3','Deleted','Deleted','system',convert(datetime,'2009-08-03 18:03:38.173',121),NULL,convert(datetime,NULL,121))
INSERT dbo.WorkFlowStatus(WorkFlowStatusId,WorkFlowSequenceId,WorkFlowStatusCode,WorkFlowStatusDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('4','4','VerificationReminderSent','Verification Reminder Sent','system',convert(datetime,'2009-08-03 18:03:38.173',121),NULL,convert(datetime,NULL,121))
INSERT dbo.WorkFlowStatus(WorkFlowStatusId,WorkFlowSequenceId,WorkFlowStatusCode,WorkFlowStatusDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('5','5','Verified','Verified','system',convert(datetime,'2009-08-03 18:03:38.173',121),NULL,convert(datetime,NULL,121))
INSERT dbo.WorkFlowStatus(WorkFlowStatusId,WorkFlowSequenceId,WorkFlowStatusCode,WorkFlowStatusDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('6','6','ApprovalReminderSent','Approval Reminder Sent','system',convert(datetime,'2009-08-03 18:03:38.190',121),NULL,convert(datetime,NULL,121))
INSERT dbo.WorkFlowStatus(WorkFlowStatusId,WorkFlowSequenceId,WorkFlowStatusCode,WorkFlowStatusDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('7','7','Approved','Approved','system',convert(datetime,'2009-08-03 18:03:38.190',121),NULL,convert(datetime,NULL,121))
INSERT dbo.WorkFlowStatus(WorkFlowStatusId,WorkFlowSequenceId,WorkFlowStatusCode,WorkFlowStatusDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('8','8','Exported','Exported','system',convert(datetime,'2009-08-03 18:03:38.190',121),NULL,convert(datetime,NULL,121))
INSERT dbo.WorkFlowStatus(WorkFlowStatusId,WorkFlowSequenceId,WorkFlowStatusCode,WorkFlowStatusDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('9','9','ExportFailed','Export Failed','system',convert(datetime,'2009-08-03 18:03:38.190',121),NULL,convert(datetime,NULL,121))
INSERT dbo.WorkFlowStatus(WorkFlowStatusId,WorkFlowSequenceId,WorkFlowStatusCode,WorkFlowStatusDescription,CreatedBy,DateCreated,ModifiedBy,DateModified) VALUES('10','10','Confirmed','Confirmed','system',convert(datetime,'2009-08-03 18:03:38.190',121),NULL,convert(datetime,NULL,121))
SET IDENTITY_INSERT dbo.WorkFlowStatus OFF;

GO



-- Set #6 - Insert Daily Rates Investments

INSERT INTO Investment (FundId, InvestmentCode, InvestmentDescription, InterimRateCode, DeclaredRateCode, GPenCode, GPenFundCode, IsDefault, PWRRule, SwitchRule, XmlFlag, InvestmentType, SuperbDeclaredCode, CalibreDeclaredCode, SuperbInterimCode, CalibreInterimCode, DateCreated, CreatedBy) SELECT FundId, 'ASCONBAL', 'AUSSUPER CONSERVATIVE BALANCED', 'ASCONBALI2', 'ASCONBALD2 / ASCONBALI2', 0, '', 0, '', '', -1, 'CR', 'AUCONBD3', 'ASCONBALD3', 'AUCONBI3', 'ASCONBALI3', getdate(), 'System' FROM Fund WHERE FundName='AUSSUPER'
INSERT INTO Investment (FundId, InvestmentCode, InvestmentDescription, InterimRateCode, DeclaredRateCode, GPenCode, GPenFundCode, IsDefault, PWRRule, SwitchRule, XmlFlag, InvestmentType, SuperbDeclaredCode, CalibreDeclaredCode, SuperbInterimCode, CalibreInterimCode, DateCreated, CreatedBy) SELECT FundId, 'AUSFIXINT', 'AUSSUPER AUSTRALIAN FIXED INTEREST', 'AUSFIXI2', 'AUSFIXD2', 9706, 'AUSTFIXINT', 0, '', '', -1, 'CR', 'AUSFIXD3', 'ASAUSFID3', 'AUSFIXI3', 'ASAUSFII3', getdate(), 'System' FROM Fund WHERE FundName='AUSSUPER'
INSERT INTO Investment (FundId, InvestmentCode, InvestmentDescription, InterimRateCode, DeclaredRateCode, GPenCode, GPenFundCode, IsDefault, PWRRule, SwitchRule, XmlFlag, InvestmentType, SuperbDeclaredCode, CalibreDeclaredCode, SuperbInterimCode, CalibreInterimCode, DateCreated, CreatedBy) SELECT FundId, 'AUSSIE', 'AUSSUPER AUSTRALIAN SHARES', 'AUSSIEI2', 'AUSSIED2', 9343, 'AUSTSHARES', 0, '', '', -1, 'CR', 'AUSSIED3', 'ASAUSSHRD3', 'AUSSIEI3', 'ASAUSSHRI3', getdate(), 'System' FROM Fund WHERE FundName='AUSSUPER'
INSERT INTO Investment (FundId, InvestmentCode, InvestmentDescription, InterimRateCode, DeclaredRateCode, GPenCode, GPenFundCode, IsDefault, PWRRule, SwitchRule, XmlFlag, InvestmentType, SuperbDeclaredCode, CalibreDeclaredCode, SuperbInterimCode, CalibreInterimCode, DateCreated, CreatedBy) SELECT FundId, 'AUSSUST', 'AUSSUPER AUSTRALIAN SUSTAINABLE SHARES', 'AUSSUSI2', 'AUSSUSD2', 9715, 'AUSSUSSHAR', 0, '', '', -1, 'CR', 'AUSSUSD3', 'ASAUSSUSD3', 'AUSSUSI3', 'ASAUSSUSI3', getdate(), 'System' FROM Fund WHERE FundName='AUSSUPER'
INSERT INTO Investment (FundId, InvestmentCode, InvestmentDescription, InterimRateCode, DeclaredRateCode, GPenCode, GPenFundCode, IsDefault, PWRRule, SwitchRule, XmlFlag, InvestmentType, SuperbDeclaredCode, CalibreDeclaredCode, SuperbInterimCode, CalibreInterimCode, DateCreated, CreatedBy) SELECT FundId, 'BALANCE', 'AUSSUPER BALANCED', 'BALI2', 'BALD2', 9339, 'Balanced', -1, '', '', -1, 'CR', 'BALD3', 'ASBALAND3', 'BALI3', 'ASBALANI3', getdate(), 'System' FROM Fund WHERE FundName='AUSSUPER'
INSERT INTO Investment (FundId, InvestmentCode, InvestmentDescription, InterimRateCode, DeclaredRateCode, GPenCode, GPenFundCode, IsDefault, PWRRule, SwitchRule, XmlFlag, InvestmentType, SuperbDeclaredCode, CalibreDeclaredCode, SuperbInterimCode, CalibreInterimCode, DateCreated, CreatedBy) SELECT FundId, 'BHPOPTC', 'AUSSUPER BHP OPTION C', 'BHPOPCI2', 'BHPOPCD2', 0, '', 0, '', '', -1, 'CR', 'BHPOPCD3', '', 'BHPOPCI3', '', getdate(), 'System' FROM Fund WHERE FundName='AUSSUPEREX'
INSERT INTO Investment (FundId, InvestmentCode, InvestmentDescription, InterimRateCode, DeclaredRateCode, GPenCode, GPenFundCode, IsDefault, PWRRule, SwitchRule, XmlFlag, InvestmentType, SuperbDeclaredCode, CalibreDeclaredCode, SuperbInterimCode, CalibreInterimCode, DateCreated, CreatedBy) SELECT FundId, 'BHPOPTD', 'AUSSUPER BHP OPTION D', 'BHPOPDI2', 'BHPOPDD2', 0, '', 0, '', '', -1, 'CR', 'BHPOPDD3', '', 'BHPOPDI3', '', getdate(), 'System' FROM Fund WHERE FundName='AUSSUPEREX'
INSERT INTO Investment (FundId, InvestmentCode, InvestmentDescription, InterimRateCode, DeclaredRateCode, GPenCode, GPenFundCode, IsDefault, PWRRule, SwitchRule, XmlFlag, InvestmentType, SuperbDeclaredCode, CalibreDeclaredCode, SuperbInterimCode, CalibreInterimCode, DateCreated, CreatedBy) SELECT FundId, 'CAPGTD', 'AUSSUPER CAPITAL GUARANTEED', 'CAPGTDI2', 'CAPGTDD2', 9342, 'CAP GUARANTEED', 0, '', '', -1, 'CR', 'CAPGTDD3', 'ASCAPGUAD3', 'CAPGTDI3', 'ASCAPGUAI3', getdate(), 'System' FROM Fund WHERE FundName='AUSSUPER'
INSERT INTO Investment (FundId, InvestmentCode, InvestmentDescription, InterimRateCode, DeclaredRateCode, GPenCode, GPenFundCode, IsDefault, PWRRule, SwitchRule, XmlFlag, InvestmentType, SuperbDeclaredCode, CalibreDeclaredCode, SuperbInterimCode, CalibreInterimCode, DateCreated, CreatedBy) SELECT FundId, 'CASHACC', 'AUSSUPER CASH', 'CASHACI2', 'CASHACD2', 9346, 'CASH AustralianSuper', 0, '', '', -1, 'CR', 'CASHACD3', 'ASCASHD3', 'CASHACI3', 'ASCASHI3', getdate(), 'System' FROM Fund WHERE FundName='AUSSUPER'
INSERT INTO Investment (FundId, InvestmentCode, InvestmentDescription, InterimRateCode, DeclaredRateCode, GPenCode, GPenFundCode, IsDefault, PWRRule, SwitchRule, XmlFlag, InvestmentType, SuperbDeclaredCode, CalibreDeclaredCode, SuperbInterimCode, CalibreInterimCode, DateCreated, CreatedBy) SELECT FundId, 'DIVFIXINT', 'AUSSUPER DIVERSIFIED FIXED INTEREST', 'DIVFIXI2', 'DIVFIXD2', 9345, 'DIVFXDINT', 0, '', '', -1, 'CR', 'DIVFIXD3', 'ASDIVFID3', 'DIVFIXI3', 'ASDIVFII3', getdate(), 'System' FROM Fund WHERE FundName='AUSSUPER'
INSERT INTO Investment (FundId, InvestmentCode, InvestmentDescription, InterimRateCode, DeclaredRateCode, GPenCode, GPenFundCode, IsDefault, PWRRule, SwitchRule, XmlFlag, InvestmentType, SuperbDeclaredCode, CalibreDeclaredCode, SuperbInterimCode, CalibreInterimCode, DateCreated, CreatedBy) SELECT FundId, 'HIGHGRTH', 'AUSSUPER HIGH GROWTH', 'HGROWI2', 'HGROWD2', 9667, 'HIGH GROWTH', 0, '', '', -1, 'CR', 'HGROWD3', 'ASHIGHGRD3', 'HGROWI3', 'ASHIGHGRI3', getdate(), 'System' FROM Fund WHERE FundName='AUSSUPER'
INSERT INTO Investment (FundId, InvestmentCode, InvestmentDescription, InterimRateCode, DeclaredRateCode, GPenCode, GPenFundCode, IsDefault, PWRRule, SwitchRule, XmlFlag, InvestmentType, SuperbDeclaredCode, CalibreDeclaredCode, SuperbInterimCode, CalibreInterimCode, DateCreated, CreatedBy) SELECT FundId, 'OSFIXINT', 'AUSSUPER INTERNATIONAL FIXED INTEREST', 'OSFIXI2', 'OSFIXD2', 9713, 'INTLFIXINT', 0, '', '', -1, 'CR', 'OSFIXD3', 'ASINTFID3', 'OSFIXI3', 'ASINTFII3', getdate(), 'System' FROM Fund WHERE FundName='AUSSUPER'
INSERT INTO Investment (FundId, InvestmentCode, InvestmentDescription, InterimRateCode, DeclaredRateCode, GPenCode, GPenFundCode, IsDefault, PWRRule, SwitchRule, XmlFlag, InvestmentType, SuperbDeclaredCode, CalibreDeclaredCode, SuperbInterimCode, CalibreInterimCode, DateCreated, CreatedBy) SELECT FundId, 'OSSUST', 'AUSSUPER INTERNATIONAL SUSTAINABLE SHARES', 'OSSUSTI2', 'OSSUSTD2', 9716, 'INTSUSSHAR', 0, '', '', -1, 'CR', 'OSSUSTD3', 'ASINTSUSD3', 'OSSUSTI3', 'ASINTSUSI3', getdate(), 'System' FROM Fund WHERE FundName='AUSSUPER'
INSERT INTO Investment (FundId, InvestmentCode, InvestmentDescription, InterimRateCode, DeclaredRateCode, GPenCode, GPenFundCode, IsDefault, PWRRule, SwitchRule, XmlFlag, InvestmentType, SuperbDeclaredCode, CalibreDeclaredCode, SuperbInterimCode, CalibreInterimCode, DateCreated, CreatedBy) SELECT FundId, 'OVERSEAS', 'AUSSUPER INTERNATIONAL SHARES', 'OVERSI2', 'OVERSD2', 9347, 'INTL SHARES', 0, '', '', -1, 'CR', 'OVERSD3', 'ASINTSHRD3', 'OVERSI3', 'ASINTSHRI3', getdate(), 'System' FROM Fund WHERE FundName='AUSSUPER'
INSERT INTO Investment (FundId, InvestmentCode, InvestmentDescription, InterimRateCode, DeclaredRateCode, GPenCode, GPenFundCode, IsDefault, PWRRule, SwitchRule, XmlFlag, InvestmentType, SuperbDeclaredCode, CalibreDeclaredCode, SuperbInterimCode, CalibreInterimCode, DateCreated, CreatedBy) SELECT FundId, 'PROPERTY', 'AUSSUPER PROPERTY', 'PROPI2', 'PROPD2', 9344, 'PROPERTY', 0, '', '', -1, 'CR', 'PROPD3', 'ASPROPD3', 'PROPI3', 'ASPROPI3', getdate(), 'System' FROM Fund WHERE FundName='AUSSUPER'
INSERT INTO Investment (FundId, InvestmentCode, InvestmentDescription, InterimRateCode, DeclaredRateCode, GPenCode, GPenFundCode, IsDefault, PWRRule, SwitchRule, XmlFlag, InvestmentType, SuperbDeclaredCode, CalibreDeclaredCode, SuperbInterimCode, CalibreInterimCode, DateCreated, CreatedBy) SELECT FundId, 'STABLE', 'AUSSUPER STABLE', 'STABLEI2', 'STABLED2', 0, '', 0, '', '', -1, 'CR', 'STABLED3', 'ASSTABLED3', 'STABLEI3', 'ASSTABLEI3', getdate(), 'System' FROM Fund WHERE FundName='AUSSUPER'
INSERT INTO Investment (FundId, InvestmentCode, InvestmentDescription, InterimRateCode, DeclaredRateCode, GPenCode, GPenFundCode, IsDefault, PWRRule, SwitchRule, XmlFlag, InvestmentType, SuperbDeclaredCode, CalibreDeclaredCode, SuperbInterimCode, CalibreInterimCode, DateCreated, CreatedBy) SELECT FundId, 'SUSTAIN', 'AUSSUPER SUSTAINABLE', 'SUSTANI2', 'SUSTAND2', 0, '', 0, '', '', -1, 'CR', 'SUSTAND3', 'ASSUSBALD3', 'SUSTANI3', 'ASSUSBALI3', getdate(), 'System' FROM Fund WHERE FundName='AUSSUPER'

GO

/*
-- Above set is generated from source data query

-- Run in DCRS MS Access database to generate the TSQL INSERT statements for the new CRS SQL 2005 database
-- Note: Two Investment OPtions (BHPOPTC and BHPOPTD) are disabled (marked to fund AUSSUPEREX), and hence will not be transferred


select
'INSERT INTO Investment ' + 
'(FundId, InvestmentCode, InvestmentDescription, InterimRateCode, DeclaredRateCode, ' +
'GPenCode, GPenFundCode, IsDefault, PWRRule, SwitchRule, XmlFlag, InvestmentType, SuperbDeclaredCode, ' +
'CalibreDeclaredCode, SuperbInterimCode, CalibreInterimCode, DateCreated, CreatedBy) ' +
'SELECT FundId, ' +
'''' + Investment_Code + ''', ' +
'''' + Investment_Description + ''', ' +
'''' + Interim_Rate_Code + ''', ' +
'''' + Declared_Rate_Code + ''', ' +
iif(isnull(GpenCode), '0', cstr(GpenCode)) + ', ' +
'''' + iif(isnull(GpenFundID), '', GpenFundID) + ''', ' +
cstr([Default]) + ', ' +
'''' + iif(isnull(PWPRule), '', PWPRule) + ''', ' +
'''' + iif(isnull(SWITCHRule), '', SWITCHRule) + ''', ' +
cstr([XML_Flag]) + ', ' +
'''' + Investment_Type + ''', ' +
'''' + iif(isnull(SuperbDeclaredCode), '', SuperbDeclaredCode) + ''', ' +
'''' + iif(isnull(CalibreDeclaredCode), '', CalibreDeclaredCode) + ''', ' +
'''' + iif(isnull(SuperbInterimCode), '', SuperbInterimCode) + ''', ' +
'''' + iif(isnull(CalibreInterimCode), '', CalibreInterimCode) + ''', ' +
'getdate(), ''System'' ' + 
'FROM Fund WHERE FundName=''' + [Fund] + '''' 
from
tblInvestmentCodes

*/



-- Set #7 - Insert Daily Rates Fund Rules

INSERT INTO FundRule (InvestmentId, RateTypeId, RatePeriodId, LastDate, RuleCode, DefaultRule, DefaultRuleCode, R2RuleCode, SuperbRuleCode, CalibreRuleCode, R2Dec, SuperbDec, CalibreDec, R2RuleType, SuperbRuleType, CalibreRuleType, CreatedBy, DateCreated) SELECT InvestmentId, 1, 4, '2008-06-30', 'D', 'BALANCE', 'D', '' , '4A', '4A', '', '4', '4', '', 'M', 'M', 'System', getdate() FROM Investment WHERE InvestmentCode='ASCONBAL'
INSERT INTO FundRule (InvestmentId, RateTypeId, RatePeriodId, LastDate, RuleCode, DefaultRule, DefaultRuleCode, R2RuleCode, SuperbRuleCode, CalibreRuleCode, R2Dec, SuperbDec, CalibreDec, R2RuleType, SuperbRuleType, CalibreRuleType, CreatedBy, DateCreated) SELECT InvestmentId, 1, 4, '2008-06-30', 'D', 'BALANCE', 'D', '' , '4A', '4A', '', '4', '4', '', 'M', 'M', 'System', getdate() FROM Investment WHERE InvestmentCode='AUSFIXINT'
INSERT INTO FundRule (InvestmentId, RateTypeId, RatePeriodId, LastDate, RuleCode, DefaultRule, DefaultRuleCode, R2RuleCode, SuperbRuleCode, CalibreRuleCode, R2Dec, SuperbDec, CalibreDec, R2RuleType, SuperbRuleType, CalibreRuleType, CreatedBy, DateCreated) SELECT InvestmentId, 1, 4, '2008-06-30', 'D', 'BALANCE', 'D', '' , '4A', '4A', '', '4', '4', '', 'M', 'M', 'System', getdate() FROM Investment WHERE InvestmentCode='AUSSIE'
INSERT INTO FundRule (InvestmentId, RateTypeId, RatePeriodId, LastDate, RuleCode, DefaultRule, DefaultRuleCode, R2RuleCode, SuperbRuleCode, CalibreRuleCode, R2Dec, SuperbDec, CalibreDec, R2RuleType, SuperbRuleType, CalibreRuleType, CreatedBy, DateCreated) SELECT InvestmentId, 1, 4, '2008-06-30', 'D', 'BALANCE', 'D', '' , '4A', '4A', '', '4', '4', '', 'M', 'M', 'System', getdate() FROM Investment WHERE InvestmentCode='AUSSUST'
INSERT INTO FundRule (InvestmentId, RateTypeId, RatePeriodId, LastDate, RuleCode, DefaultRule, DefaultRuleCode, R2RuleCode, SuperbRuleCode, CalibreRuleCode, R2Dec, SuperbDec, CalibreDec, R2RuleType, SuperbRuleType, CalibreRuleType, CreatedBy, DateCreated) SELECT InvestmentId, 1, 4, '2008-06-30', 'D', 'BALANCE', 'D', '4A' , '4A', '4A', '4', '4', '4', 'M', 'M', 'M', 'System', getdate() FROM Investment WHERE InvestmentCode='BALANCE'
INSERT INTO FundRule (InvestmentId, RateTypeId, RatePeriodId, LastDate, RuleCode, DefaultRule, DefaultRuleCode, R2RuleCode, SuperbRuleCode, CalibreRuleCode, R2Dec, SuperbDec, CalibreDec, R2RuleType, SuperbRuleType, CalibreRuleType, CreatedBy, DateCreated) SELECT InvestmentId, 1, 4, '2008-06-30', 'D', 'BALANCE', 'D', '' , '4A', '', '', '4', '', '', 'M', '', 'System', getdate() FROM Investment WHERE InvestmentCode='BHPOPTC'
INSERT INTO FundRule (InvestmentId, RateTypeId, RatePeriodId, LastDate, RuleCode, DefaultRule, DefaultRuleCode, R2RuleCode, SuperbRuleCode, CalibreRuleCode, R2Dec, SuperbDec, CalibreDec, R2RuleType, SuperbRuleType, CalibreRuleType, CreatedBy, DateCreated) SELECT InvestmentId, 1, 4, '2008-06-30', 'D', 'BALANCE', 'D', '' , '4A', '', '', '4', '', '', 'M', '', 'System', getdate() FROM Investment WHERE InvestmentCode='BHPOPTD'
INSERT INTO FundRule (InvestmentId, RateTypeId, RatePeriodId, LastDate, RuleCode, DefaultRule, DefaultRuleCode, R2RuleCode, SuperbRuleCode, CalibreRuleCode, R2Dec, SuperbDec, CalibreDec, R2RuleType, SuperbRuleType, CalibreRuleType, CreatedBy, DateCreated) SELECT InvestmentId, 1, 4, '2008-06-30', 'D', 'BALANCE', 'D', '' , '4A', '4A', '', '4', '4', '', 'M', 'M', 'System', getdate() FROM Investment WHERE InvestmentCode='CAPGTD'
INSERT INTO FundRule (InvestmentId, RateTypeId, RatePeriodId, LastDate, RuleCode, DefaultRule, DefaultRuleCode, R2RuleCode, SuperbRuleCode, CalibreRuleCode, R2Dec, SuperbDec, CalibreDec, R2RuleType, SuperbRuleType, CalibreRuleType, CreatedBy, DateCreated) SELECT InvestmentId, 1, 4, '2008-06-30', 'D', 'BALANCE', 'D', '' , '4A', '4A', '', '4', '4', '', 'M', 'M', 'System', getdate() FROM Investment WHERE InvestmentCode='CASHACC'
INSERT INTO FundRule (InvestmentId, RateTypeId, RatePeriodId, LastDate, RuleCode, DefaultRule, DefaultRuleCode, R2RuleCode, SuperbRuleCode, CalibreRuleCode, R2Dec, SuperbDec, CalibreDec, R2RuleType, SuperbRuleType, CalibreRuleType, CreatedBy, DateCreated) SELECT InvestmentId, 1, 4, '2008-06-30', 'D', 'BALANCE', 'D', '' , '4A', '4A', '', '4', '4', '', 'M', 'M', 'System', getdate() FROM Investment WHERE InvestmentCode='DIVFIXINT'
INSERT INTO FundRule (InvestmentId, RateTypeId, RatePeriodId, LastDate, RuleCode, DefaultRule, DefaultRuleCode, R2RuleCode, SuperbRuleCode, CalibreRuleCode, R2Dec, SuperbDec, CalibreDec, R2RuleType, SuperbRuleType, CalibreRuleType, CreatedBy, DateCreated) SELECT InvestmentId, 1, 4, '2008-06-30', 'D', 'BALANCE', 'D', '' , '4A', '4A', '', '4', '4', '', 'M', 'M', 'System', getdate() FROM Investment WHERE InvestmentCode='HIGHGRTH'
INSERT INTO FundRule (InvestmentId, RateTypeId, RatePeriodId, LastDate, RuleCode, DefaultRule, DefaultRuleCode, R2RuleCode, SuperbRuleCode, CalibreRuleCode, R2Dec, SuperbDec, CalibreDec, R2RuleType, SuperbRuleType, CalibreRuleType, CreatedBy, DateCreated) SELECT InvestmentId, 1, 4, '2008-06-30', 'D', 'BALANCE', 'D', '' , '4A', '4A', '', '4', '4', '', 'M', 'M', 'System', getdate() FROM Investment WHERE InvestmentCode='OSFIXINT'
INSERT INTO FundRule (InvestmentId, RateTypeId, RatePeriodId, LastDate, RuleCode, DefaultRule, DefaultRuleCode, R2RuleCode, SuperbRuleCode, CalibreRuleCode, R2Dec, SuperbDec, CalibreDec, R2RuleType, SuperbRuleType, CalibreRuleType, CreatedBy, DateCreated) SELECT InvestmentId, 1, 4, '2008-06-30', 'D', 'BALANCE', 'D', '' , '4A', '4A', '', '4', '4', '', 'M', 'M', 'System', getdate() FROM Investment WHERE InvestmentCode='OSSUST'
INSERT INTO FundRule (InvestmentId, RateTypeId, RatePeriodId, LastDate, RuleCode, DefaultRule, DefaultRuleCode, R2RuleCode, SuperbRuleCode, CalibreRuleCode, R2Dec, SuperbDec, CalibreDec, R2RuleType, SuperbRuleType, CalibreRuleType, CreatedBy, DateCreated) SELECT InvestmentId, 1, 4, '2008-06-30', 'D', 'BALANCE', 'D', '' , '4A', '4A', '', '4', '4', '', 'M', 'M', 'System', getdate() FROM Investment WHERE InvestmentCode='OVERSEAS'
INSERT INTO FundRule (InvestmentId, RateTypeId, RatePeriodId, LastDate, RuleCode, DefaultRule, DefaultRuleCode, R2RuleCode, SuperbRuleCode, CalibreRuleCode, R2Dec, SuperbDec, CalibreDec, R2RuleType, SuperbRuleType, CalibreRuleType, CreatedBy, DateCreated) SELECT InvestmentId, 1, 4, '2008-06-30', 'D', 'BALANCE', 'D', '' , '4A', '4A', '', '4', '4', '', 'M', 'M', 'System', getdate() FROM Investment WHERE InvestmentCode='PROPERTY'
INSERT INTO FundRule (InvestmentId, RateTypeId, RatePeriodId, LastDate, RuleCode, DefaultRule, DefaultRuleCode, R2RuleCode, SuperbRuleCode, CalibreRuleCode, R2Dec, SuperbDec, CalibreDec, R2RuleType, SuperbRuleType, CalibreRuleType, CreatedBy, DateCreated) SELECT InvestmentId, 1, 4, '2008-06-30', 'D', 'BALANCE', 'D', '' , '4A', '4A', '', '4', '4', '', 'M', 'M', 'System', getdate() FROM Investment WHERE InvestmentCode='STABLE'
INSERT INTO FundRule (InvestmentId, RateTypeId, RatePeriodId, LastDate, RuleCode, DefaultRule, DefaultRuleCode, R2RuleCode, SuperbRuleCode, CalibreRuleCode, R2Dec, SuperbDec, CalibreDec, R2RuleType, SuperbRuleType, CalibreRuleType, CreatedBy, DateCreated) SELECT InvestmentId, 1, 4, '2008-06-30', 'D', 'BALANCE', 'D', '' , '4A', '4A', '', '4', '4', '', 'M', 'M', 'System', getdate() FROM Investment WHERE InvestmentCode='SUSTAIN'

GO

/*
-- Above set is generated from source data query

-- Run in DCRS MS Access database to generate the TSQL INSERT statements for the new CRS SQL 2005 database
-- Note: Two Fund Rules for Investment Options (BHPOPTC and BHPOPTD) will not be transferred

select
'INSERT INTO FundRule ' + 
'(InvestmentId, RateTypeId, RatePeriodId, LastDate, RuleCode, DefaultRule, ' + 
'DefaultRuleCode, R2RuleCode, SuperbRuleCode, CalibreRuleCode, R2Dec, SuperbDec, ' + 
'CalibreDec, R2RuleType, SuperbRuleType, CalibreRuleType, CreatedBy, DateCreated) ' +
'SELECT InvestmentId, ' +
'1, ' +
'4, ' +
'''' + format(dLastDate, 'yyyy-mm-dd') + ''', ' +
'''' + RuleCode + ''', ' +
'''' + Default + ''', ' +
'''' + DefaultRuleCode + ''', ' +
'''' + iif(isnull(R2RuleCode), '', R2RuleCode) + ''' , ' +
'''' + iif(isnull(SuperbRuleCode), '', SuperbRuleCode) + ''', ' +
'''' + iif(isnull(CalibreRuleCode), '', CalibreRuleCode) + ''', ' +
'''' + iif(isnull(R2Dec), '', R2Dec) + ''', ' +
'''' + iif(isnull(SuperbDec), '', SuperbDec) + ''', ' +
'''' + iif(isnull(CalibreDec), '', cstr(CalibreDec)) + ''', ' +
'''' + iif(isnull(R2RuleType), '', R2RuleType) + ''', ' +
'''' + iif(isnull(SuperbRuleType), '', SuperbRuleType) + ''', ' +
'''' + iif(isnull(CalibreRuleType), '', CalibreRuleType) + ''', ' + 
'''System'', getdate() ' + 
'FROM Investment WHERE InvestmentCode=''' + Investment_Code + '''' 
from
tblFundRules

*/

