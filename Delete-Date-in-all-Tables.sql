--make changes for task 14 in JIRA

delete from dbo.CRS_Access_Rates
delete from dbo.ExportQueue
delete from dbo.ProcessLog
delete from dbo.RateAudit
delete from dbo.ReconTempRates
delete from dbo.Schedule
delete from dbo.Task
delete from dbo.WorkFlowHistory
--delete from dbo.SPIT_Rates
delete from dbo.TransitionRate
delete from dbo.XmlMethodParameters
delete from dbo.UserFunds
delete from dbo.SystemConfig

delete from dbo.Rate
delete from dbo.WorkFlow
delete from dbo.WorkFlowStatus
delete from dbo.FundRule
delete from dbo.Investment
delete from dbo.Fund
delete from dbo.RatePeriod
delete from dbo.RateType

